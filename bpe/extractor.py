#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.asmmain import Asm
from bpe.exe import Exe
from bpe.funcs import toUtf8

import os
import re

packetVersionRe = re.compile(
    r"^(?P<v1>[\d][\d][\d][\d])-(?P<v2>[\d][\d])-(?P<v3>[\d][\d])")


class Extractor:
    from bpe.extractors.accountid import searchAccountId
    from bpe.extractors.actres import searchActRes
    from bpe.extractors.basicinfo import getBasicInfo
    from bpe.extractors.ccurlmgr import searchCCurlMgr
    from bpe.extractors.cfile import searchCFile
    from bpe.extractors.cfileopen import searchCFileOpen
    from bpe.extractors.cfileopen1 import searchCFileOpen1
    from bpe.extractors.charcreatepacket import searchCharCreatePacket
    from bpe.extractors.charinfo import searchCharInfo
    from bpe.extractors.combo import searchCombo
    from bpe.extractors.connect import searchConnect
    from bpe.extractors.cheatdefender import searchCheatDefender
    from bpe.extractors.connectionstartup import searchConnectionStartup
    from bpe.extractors.constructarray import searchConstructArray
    from bpe.extractors.errormsg import searchErrorMsg
    from bpe.extractors.functiontypes import setFunctionTypes
    from bpe.extractors.getgamemode import searchGetGameMode
    from bpe.extractors.msgstrs import searchMsgStrs, addMsgStrLabels
    from bpe.extractors.getpacketsizes import searchGetPacketSizes, \
        addGetPacketSizeLabels
    from bpe.extractors.globaltypes import setGlobalTypes
    from bpe.extractors.iteminfo import searchItemInfo
    from bpe.extractors.iteminfofunctions import searchItemInfoFunctions
    from bpe.extractors.language import searchLanguage
    from bpe.extractors.loginpacket import searchLoginPacketHandler
    from bpe.extractors.loginpackets import searchLoginPackets
    from bpe.extractors.lua import searchLua
    from bpe.extractors.lua2 import searchLua2
    from bpe.extractors.lua3 import searchLua3
    from bpe.extractors.luacustom import searchLuaCustom
    from bpe.extractors.makewindows import searchMakeWindows, \
        addMakeWindowLabels
    from bpe.extractors.mappacket import searchMapPacketHandler
    from bpe.extractors.mappackets import searchMapPackets
    from bpe.extractors.maxchars import searchMaxChars
    from bpe.extractors.membertypes import setMemberTypes
    from bpe.extractors.modemgr import searchModeMgr
    from bpe.extractors.msgstringtable import searchMsgStringTable, \
        extractMsgStrings
    from bpe.extractors.oep import searchOEP
    from bpe.extractors.packet73 import searchPacket73
    from bpe.extractors.packetbuf import searchPacketBuf
    from bpe.extractors.recvpacket import searchRecvPacket
    from bpe.extractors.renderer import searchRenderer
    from bpe.extractors.resmgr import searchResMgr
    from bpe.extractors.sendencryption import searchSendEncryption
    from bpe.extractors.sendpacketold import searchSendPacketOld
    from bpe.extractors.sendpacketold2 import searchSendPacketOld2
    from bpe.extractors.sendpackets import searchSendPackets, \
        addSendPacketLabels
    from bpe.extractors.servertype import searchServerType
    from bpe.extractors.servicetype import searchServiceType
    from bpe.extractors.sendpacket import searchSendPacket
    from bpe.extractors.session import searchSession
    from bpe.extractors.sessionparams import searchSessionParams
    from bpe.extractors.shuffle0 import searchShuffle0
    from bpe.extractors.shuffle23 import searchShuffle23
    from bpe.extractors.skinmgr import searchSkinMgr
    from bpe.extractors.soundmgr import searchSoundMgr
    from bpe.extractors.soundmgrplaywave import searchSoundMgrPlayWave
    from bpe.extractors.stdstring import searchStdString
    from bpe.extractors.stdstringconstructor import searchStdStringConstructor
    from bpe.extractors.stdstringfind import searchStdStringFind
    from bpe.extractors.stdstringsprintf import searchStdStringSprintf
    from bpe.extractors.talktypetable import searchTalkTypeTable
    from bpe.extractors.version import searchVersion
    from bpe.extractors.vtbl import searchVtbl, vtblNameMembers, \
        vtblAddStruct, vtblApplyMemberParams, vtblApplyVtblTypes
    from bpe.extractors.windowmgr import searchWindowMgr
    from bpe.extractors.windowmgrdeletewindow import \
        searchWindowMgrDeleteWindow
    from bpe.extractors.windowmgrdeletewindow1 import \
        searchWindowMgrDeleteWindow1
    from bpe.extractors.windowmgrparams import searchWindowMgrParams
    from bpe.vars import showRawAddr, showVaAddr, addVaCommentAddr, \
        addRawFunc, addVaFunc, addRawVar, addVaVar, addVaVarStr, addRawArray, \
        addVaArray, addStruct, addStructMember, makeUnknown, makeDword, \
        addVaStruct, setStructMemberType, setStructComment, \
        setStructMemberComment, addVaLabel, makeVaStruct, setVarType, \
        setVaFuncType, setRawFuncType, setVaFuncParam, setVtblMemberType, \
        addVaFuncType, addRawFuncType, setFuncCallPrefix, getVaAddrToName, \
        removeVaFunc, isVarPresent, getAddrByName

    def __init__(self):
        self.offset1 = None
        self.hookAddr = 0
        self.hookAddr2 = 0
        self.windowFunc = 0
        # list of addr objects (names, comments, etc)
        self.addrList = []
        # dict of addr objects names. addr=addrObj
        self.addrNameDict = dict()
        # name = addr
        self.nameAddrDict = dict()
        self.strcutsDefined = set()
        self.ollyScript = ""
        self.loginPollAddrVa = 0
        self.gamePollAddrVa = 0
        self.connectionStartup = 0
        self.connectionStartupVa = 0
        self.sendPacket = 0
        self.sendAddr = 0
        self.recvAddr = 0
        self.packetParser1BytesCount = 16
        self.packetParser2BytesCount = 16
        self.packetParser3BytesCount = 16
        self.packetParser4BytesCount = 16
        self.packetParser5BytesCount = 16
        self.instanceR = 0
        self.g_instanceR = 0
        self.cleanFileName = ""
        self.gCheatDefenderMgr = 0
        self.cheatDefenderMgrInit = 0
        self.packetBuf = 0
        self.hookRecvPacketStartMap = 0
        self.hookRecvPacketExitMap = 0
        self.callRecvPacket1 = 0
        self.g_modeMgr = 0
        self.CGameMode_mWorldOffset = 0
        self.getPacketSizeFunction = 0
        self.vtables = None
        self.comboSimple = False
        self.getPacketSizeBlocks = dict()
        self.maxChars = 0
        self.gWindowMgr = 0
        # addr = (funcName, thisParamName)
        self.paramFunctions = dict()
        self.luaTables = dict()
        self.lua_getfield = 0
        self.session = 0
        self.soundMgr = 0
        # keys is parameters to this function (3 keys)
        self.encryptionInitKeys = 0
        # keys inside this function (4 keys)
        self.encryptionInitKeys0 = 0
        self.encPacketKeys = 0
        self.setBlock = 0
        self.clientType = "kro"
        self.ITEM_INFO_init = 0
        self.ITEM_INFO_SetItemId = 0
        self.ITEM_INFO_GetIdDisplayName = 0
        self.key1 = -1
        self.gLanguage = 0
        self.commands = []
        self.packet73Tmp = False
        self.MsgStr = 0
        self.fprintf = 0
        self.CLoginMode_m_charInfo = 0
        self.cFileMode = 0
        self.CFileMgr_readFileWithFlagCall = 0
        # struct name = (member=addr)
        self.structs = dict()
        self.stdstringSize = -1
        self.stdstringAllocLen = -1
        self.stdstring_constructor = 0
        self.CCURLMgr_handle = 0
        self.g_CCURLMgr = 0
        self.CCURLMgr_curl_init = False
        self.CCURLMgrInitBlock = 0
        self.CCURLMgrInitBlockVa = 0
        self.msgStringTable = 0
        self.msgStrings = None
        self.msgStrBlocks = None


    def close(self):
        self.exe.close()
        self.vtables = None
        self.addrNameDict = None
        self.nameAddrDict = None
        self.strcutsDefined = set()
        self.msgStrings = None
        self.getPacketSizeBlocks = dict()
        self.paramFunctions = dict()
        self.luaTables = dict()


    def setExe(self, exe):
        self.exe = exe
        self.log = exe.log
        self.getAddr = exe.getAddr
        self.getAddrList = exe.getAddrList
        self.getVarAddrInt = exe.getVarAddrInt
        self.getVarAddrByte = exe.getVarAddrByte
        self.getVarAddr = exe.getVarAddr


    def runAsmSimple(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(0)
        # asm.printRegisters()
        # asm.printLine()
        asm.run()
        # asm.printRegisters()
        while asm.move() is True:
            # asm.printLine()
            asm.run()
            # asm.printRegisters()
            pass
        self.stackAlign = asm.popCounter
        self.callFunctions = asm.callFunctions
        self.allCallFunctions = asm.callFunctions
        self.allCallAddrs = asm.allCallAddrs
        print("Asm simple code complete")
        # print(asm.memory)


    def runAsmCollect(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.stackAlign = self.stackAlign
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(1)
        asm.callFunctions = self.callFunctions
        asm.allCallFunctions = self.allCallFunctions
        # asm.printRegisters()
        # asm.printMemory()
        # asm.printLine()
        asm.run()
        while asm.move() is True:
            asm.run()
            pass
        self.packets = asm.packets
        self.callFunctions = asm.callFunctions
        print("Asm collector code complete")


    def parseCallStack(self):
        correctFunctions = dict()
        for addr in self.callFunctions:
            callFunction = self.callFunctions[addr]
            if callFunction.callCounter < 10:
                continue
            correctCounts = dict()
            tmpPushes = 0
            # walk all push counts and remove used very rare
            for pushCount in callFunction.counts:
                num = callFunction.counts[pushCount]
                if num > 10:
                    correctCounts[pushCount] = num
                    tmpPushes = pushCount
            callFunction.counts = correctCounts
            # need at least two different function calls
            # in initPacketMapWithClient
            if len(callFunction.counts) > 1:
                self.log("Error. Different number of pushes before " +
                         "function call: {0}\n{1}".
                         format(self.exe.getAddrSec(addr),
                                callFunction.counts))
                exit(1)
            callFunction.adjustSp = tmpPushes
            # print("addr {0}, pushes {1}".format(hex(self.exe.rawToVa(addr)),
            #                                     callFunction.adjustSp))
            correctFunctions[addr] = callFunction
        self.callFunctions = correctFunctions


    def checkPackets(self):
        error = False
        for key in self.callFunctions:
            func = self.callFunctions[key]
            if len(func.collectCallsFrom) < 1:
                continue
            for calladdr in func.callsFrom:
                if calladdr not in func.collectCallsFrom:
                    print("Error function was not called from addr: {0}, {1}".
                          format(self.exe.getAddrSec(key),
                                 self.exe.getAddrSec(calladdr)))
                    error = True
        if error is True:
            exit(1)


        if len(self.packets) < 380:
            self.log("Error. too small packets amount: {0}".format(
                len(self.packets)))
            exit(1)


    def savePackets(self):
        self.cleanupName(self.exe.fileName)
        self.log("Packets number: {0}".format(len(self.packets)))
        processedKeys = dict()
        outDir = "output/packets/{0}".format(self.cleanFileName)
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        with open("{0}/bpe_data_{1}.ini".format(outDir,
                                                self.exe.client_date
                                                ), "wt") as w:
            for packet in self.packets:
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.log("Warning. Duplicate packet with " +
                             "different size: {0}: {1} vs {2}".
                             format(hex(packet[0]),
                                    packet[1],
                                    newLen
                                    ))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0},{1},{2},{3}\n".format(packetId,
                                                   packet[1],
                                                   packet[2],
                                                   packet[3]))
                processedKeys[packet[0]] = packet
        processedKeys = dict()
        with open("{0}/bpe_PacketLengths_{1}.ini".format(outDir,
                                                         self.exe.client_date
                                                         ), "wt") as w:
            w.write("[Packet_Lengths]\r\n")
            for packet in self.packets:
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.log("Warning. Duplicate packet with different " +
                             "size: {0}: {1} vs {2}".
                             format(hex(packet[0]),
                                    packet[1],
                                    newLen))
                    continue
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0} = {1}\r\n".format(packetId, packet[1]))
                processedKeys[packet[0]] = packet
            w.write("[Shuffle_Packets]\r\n")


    def getHexAddr(self, addr):
        addr = hex(addr)[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def getSimpleAddr(self, addr):
        addr = hex(self.exe.rawToVa(addr))[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def addScriptCommand(self, w, cmd):
        w.write(cmd + "\n")
        self.ollyScript = self.ollyScript + cmd + "\n"
        print(cmd)


    def addScriptCommand2(self, w, cmd):
        w.write(cmd + "\n")
        print(cmd)


    def cleanupName(self, fileName):
        if fileName[-2:] == "_D":
            fileName = fileName[:-2]
        if fileName[-5:] == "_dump":
            fileName = fileName[:-5]
        if fileName[-3:] == "_DP":
            fileName = fileName[:-3]
        fileName = fileName.replace("ragexe", "Ragexe")
        self.cleanFileName = fileName


    def saveClasses(self):
        self.cleanupName(self.exe.fileName)
        outDir = "output/classes/"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        names = []
        for obj in self.vtables:
            offsets = []
            if obj.vtblOffset != 0:
                offsets.append("vtblOffset={0}".format(obj.vtblOffset))
            if obj.constructorOffset != 0:
                offsets.append("constructorOffset={0}".
                               format(obj.constructorOffset))
            if obj.attr != 0:
                offsets.append("attr={0}".format(obj.attr))
            offsetStr = ",".join(offsets)
            if offsetStr != "":
                offsetStr = " (" + offsetStr + ")"
            formatStr = "{name} {type}{extra}: " + \
                        "{offset},{members} {base}\n"
            names.append(formatStr.format(name=obj.name,
                                          type=obj.typeStr,
                                          offset=hex(obj.offsetV + 4),
                                          members=len(obj.members),
                                          base=obj.baseClassesStr,
                                          extra=offsetStr))
        with open("{0}{1}.txt".format(outDir, self.cleanFileName), "wt") as w:
            for name in sorted(names):
                w.write(name)
        self.log("Saved {0} classes".format(len(self.vtables)))


    def saveMsgString(self):
        self.cleanupName(self.exe.fileName)
        outDir = "output/msgstringtable/" + self.cleanFileName
        if os.path.basename(os.path.abspath(".")) == "zero":
            outDir = outDir + "_zero"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        separator = b"-" * 80
        with open("{0}/msgstringtable_text.txt".format(outDir), "wb") as wt:
            with open("{0}/msgstringtable_hex.txt".format(outDir), "w") as wh:
                with open("{0}/msgstringtable.txt".format(outDir), "wb") as wm:
                    for key in self.msgStrings:
                        msg = self.msgStrings[key]
                        wt.write(str(key).encode())
                        wt.write(b" (")
                        wt.write(hex(key).encode())
                        wt.write(b"):\n")
                        wt.write(toUtf8(msg))
                        wt.write(b"\n")
                        wt.write(separator)
                        wt.write(b"\n")
                        try:
                            msgHex = msg.hex()
                        except AttributeError:
                            msgHex = msg.encode("hex")
                        wh.write("{0}: {1}\n".format(hex(key), msgHex))
                        msg = msg.replace(b"#", b"_")
                        msg = msg.replace(b"\n", b"\\n")
                        msg = msg.replace(b"\r", b"\\r")
                        wm.write(msg)
                        wm.write(b"#\n")


    def saveSendPackets(self):
        packets = set()
        for block in self.getPacketSizeBlocks:
            addr = hex(self.exe.rawToVa(block[1]))
            packetId = hex(block[2])
            packets.add("{0} - {1}".format(packetId, addr))
        for block in self.sendPacketBlocks:
            addr = hex(self.exe.rawToVa(block[1]))
            packetId = hex(block[2])
            packets.add("{0} - {1}".format(packetId, addr))
        packets = sorted(packets)
        self.cleanupName(self.exe.fileName)
        outDir = "output/sendpackets/" + self.cleanFileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        with open("{0}/sendpackets.txt".format(outDir), "w") as w:
            w.write("# size: {0},{1}; send: {2},{3}\n".
                    format(self.getPacketSizeStats[0],
                           self.getPacketSizeStats[1],
                           self.sendPacketStats[0],
                           self.sendPacketStats[1]))
            for packet in packets:
                w.write(packet)
                w.write("\n")


    def saveKeys(self):
        if self.key1 == -1:
            self.log("Error: keys not found")
            if self.packetVersion < "20130821" and \
               self.packetVersion >= "20120000":
                exit(1)
            return
        self.cleanupName(self.exe.fileName)
        outDir = "output/keys/" + self.cleanFileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        with open("{0}/keys.txt".format(outDir), "w") as w:
            w.write("keys: {0},{1},{2} - 0\n".
                    format(hex(self.key1), hex(self.key2), hex(self.key3)))


    def init(self, fileName, outFileName):
        exe = Exe()
        if exe.load(fileName, outFileName) is False:
            print("Skipping file {0}".format(fileName))
            exit(1)
        m = packetVersionRe.search(fileName)
        if m is not None:
            self.packetVersion = m.group("v1") + m.group("v2") + m.group("v3")
        else:
            self.packetVersion = "0000000"
        self.setExe(exe)
        name = os.path.basename(os.path.abspath("."))
        if name == "zero":
            self.clientType = "zero"
        elif name == "mro":
            self.clientType = "mro"
        elif name == "iro":
            self.clientType = "iro"
        elif name == "ruro":
            self.clientType = "ruro"
        elif name == "bro":
            self.clientType = "bro"
        elif name == "euro":
            self.clientType = "euro"
        elif name == "tro":
            self.clientType = "tro"
        else:
            self.clientType = "kro"


    def makeOutDir(self):
        if not os.path.exists("output"):
            os.makedirs("output")
        if not os.path.exists(".cache"):
            os.makedirs(".cache")


    def searchBasicInfo(self):
        parsed = self.getBasicInfo()
        if parsed is False:
            self.log("Error: basic parsing info failed")
            exit(1)


    def walkUntilRet(self):
        asm = Asm()
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        # asm.printLine()
        while asm.move() is True:
            # asm.printLine()
            pass
        self.asmEndPos = asm.pos
        self.showRawAddr("Last asm walk addr", self.asmEndPos)
        print("Asm code walked")


    def getpackets(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchBasicInfo()
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.runAsmCollect()
        self.checkPackets()
        self.savePackets()


    def getInfo(self):
        self.makeOutDir()
        self.exe.cache.loadAll()
        self.searchVtbl(True)
        self.vtblNameMembers()
        self.searchBasicInfo()
        self.searchConnect()
        self.searchActRes()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
        self.searchMapPackets()
        self.searchLoginPackets()
        self.searchSendPacketOld(True)
        self.searchSendPacketOld2(True)
        self.searchSendPacket(1)
        self.searchPacketBuf(False)
        self.searchMaxChars(False)
        self.searchCharInfo(True)
        self.searchCharCreatePacket(True)
        self.searchRecvPacket()
        self.searchCombo()
        self.searchOEP()
        self.searchShuffle0(False)
        self.searchCheatDefender(False)
        self.searchSendEncryption(True)
        self.searchWindowMgr(False)
        self.searchWindowMgrDeleteWindow(True)
        self.searchWindowMgrDeleteWindow1(True)
        self.searchVersion()
        self.searchMakeWindows()
        self.searchTalkTypeTable()
        self.searchServiceType(False)
        self.searchServerType(False)
        self.searchMsgStringTable()
        self.searchConnectionStartup(False)
        self.searchErrorMsg(True)
        self.searchGetGameMode()
        self.searchModeMgr()
        self.searchSession(True)
        self.searchShuffle23(False)
        self.searchAccountId()
        self.searchCCurlMgr()
        self.searchCFile()
        self.searchPacket73()
        self.searchStdString()
        self.searchStdStringFind()
        self.searchStdStringSprintf()
        self.searchStdStringConstructor()
        self.searchCFileOpen()
        self.searchCFileOpen1()
        self.searchItemInfoFunctions()
        self.searchItemInfo()
        self.searchLanguage()
        self.searchConstructArray()
        self.searchSessionParams(False)
        self.searchWindowMgrParams(False)
        self.searchSkinMgr(True)
        self.searchResMgr(True)
        self.searchSoundMgr(False)
        self.searchSoundMgrPlayWave()
        self.searchRenderer(True)
        self.searchLua(False)
        self.searchLuaCustom(False)
        self.searchLua2(False)
        self.searchLua3()
        self.searchGetPacketSizes()
        self.searchSendPackets()
        self.extractMsgStrings()
        self.searchMsgStrs()
        self.addGetPacketSizeLabels()
        self.addSendPacketLabels()
        self.addMsgStrLabels()
        self.addMakeWindowLabels()
        self.vtblAddStruct()
        self.vtblApplyMemberParams()
        self.setGlobalTypes()
        self.setMemberTypes()
        self.setFunctionTypes()
        self.vtblApplyVtblTypes()
        self.exe.cache.saveAll()


    def getIda(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchBasicInfo()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()


    def getMaxChars(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchMaxChars(True)


    def getClass(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchVtbl(True)
        self.saveClasses()


    def getMsgStringTable(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchServiceType(True)
        self.searchMsgStringTable()
        self.extractMsgStrings()
        self.saveMsgString()


    def getSendPackets(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchBasicInfo()
        self.searchLoginPacketHandler()
        self.searchSendPacketOld(True)
        self.searchSendPacketOld2(True)
        self.searchGetPacketSizes()
        self.searchSendPackets()
        self.saveSendPackets()


    def getKeys(self):
        self.makeOutDir()
        self.exe.cache.clear()
        self.searchBasicInfo()
        self.searchLoginPacketHandler()
        self.searchSendPacketOld(True)
        self.searchSendPacketOld2(True)
        self.searchCombo()
        self.searchGetGameMode()
        self.searchModeMgr()
        self.searchSession(True)
        self.searchShuffle23(False)
        self.saveKeys()


    def getDebug(self):
        self.makeOutDir()
        self.exe.cache.loadAll()
        self.searchActRes()
        self.exe.cache.saveAll()
