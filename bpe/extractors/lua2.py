#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchLua2(self, errorExit):
    offset, _ = self.exe.string(b"Lua 5.1")
    if offset is False:
        self.log("Error: Lua 5.1 not found")
        if errorExit is True and self.packetVersion >= "20120110" and \
           self.clientType != "iro":
            exit(1)
        return

    offset, section = self.exe.string(b"system\\Achievement_list.lub")
    if offset is False:
        self.log("Error: system\\Achievement_list.lub not found")
        if errorExit is True:
            exit(1)
        return
    achListStringHex = self.exe.toHex(section.rawToVa(offset), 4)

    offset, section = self.exe.string(b"CAchievementMgr")
    if offset is False:
        self.log("Error: CAchievementMgr not found")
        exit(1)
    achievementMgrString = self.exe.toHex(section.rawToVa(offset), 4)

    # 0  push offset filename
    # 5  push [ebp+var_18.L]
    # 8  call luaL_loadfile
    # 13 add esp, 2Ch
    code = (
        b"\x68" + achListStringHex +       # 0 push offset filename
        b"\xFF\x75\xAB"                    # 5 push [ebp+var_18.L]
        b"\xE8\xAB\xAB\xAB\xAB"            # 8 call luaL_loadfile
        b"\x83\xC4\x2C"                    # 13 add esp, 2Ch
    )
    loadFileOffset = 9
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov eax, [ebp+L]
        # 3  push offset aSystemAchievem
        # 8  push eax
        # 9  call luaL_loadfile
        # 14 add esp, 2Ch
        code = (
            b"\x8B\x45\xAB"                    # 0 mov eax, [ebp+L]
            b"\x68" + achListStringHex +       # 3 push offset aSystemAchievem
            b"\x50"                            # 8 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 9 call luaL_loadfile
            b"\x83\xC4\x2C"                    # 14 add esp, 2Ch
        )
        loadFileOffset = 10
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: system\\Achievement_list.lub usage not found")
        exit(1)

    self.luaL_loadfile = self.getAddr(offset,
                                      loadFileOffset,
                                      loadFileOffset + 4)

    self.addRawFuncType("luaL_loadfile",
                        self.luaL_loadfile,
                        "int __cdecl luaL_loadfile" +
                        "(void *L, const char *filename)")

    # 0  push offset aCachievementmg
    # 5  push 0
    # 7  push 0FFFFFFFFh
    # 9  push [ebp+var_18.L]
    # 12 call lua_tolstring
    # 17 add esp, 0Ch
    # 20 push eax
    # 21 push 0
    # 23 call MessageBoxA
    code = (
        b"\x68" + achievementMgrString +   # 0 push offset aCachievementmg
        b"\x6A\x00"                        # 5 push 0
        b"\x6A\xFF"                        # 7 push 0FFFFFFFFh
        b"\xFF\x75\xAB"                    # 9 push [ebp+var_18.L]
        b"\xE8\xAB\xAB\xAB\xAB"            # 12 call lua_tolstring
        b"\x83\xC4\x0C"                    # 17 add esp, 0Ch
        b"\x50"                            # 20 push eax
        b"\x6A\x00"                        # 21 push 0
        b"\xFF\x15"                        # 23 call MessageBoxA
    )
    lua_tolstringOffset = 13
    MessageBoxAOffset = 25
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push offset aCachievementmg
        # 5  push 0
        # 7  push 0FFFFFFFFh
        # 9  push [ebp+L]
        # 12 call sub_7FAFA0
        # 17 add esp, 0Ch
        # 20 push eax
        # 21 push 0
        # 23 call near ptr 3CB022Fh
        # 28 no nop
        code = (
            b"\x68" + achievementMgrString +   # 0 push offset aCachievementmg
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\xFF"                        # 7 push 0FFFFFFFFh
            b"\xFF\x75\xAB"                    # 9 push [ebp+var_18.L]
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call lua_tolstring
            b"\x83\xC4\x0C"                    # 17 add esp, 0Ch
            b"\x50"                            # 20 push eax
            b"\x6A\x00"                        # 21 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 23 call near ptr 3CB022Fh
            b"\x90"                            # 28 no nop
        )
        lua_tolstringOffset = 13
        MessageBoxAOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push offset aCachievementmg
        # 5  push 0
        # 7  push 0FFFFFFFFh
        # 9  push ecx
        # 10 call lua_tolstring
        # 15 add esp, 0Ch
        # 18 push eax
        # 19 push 0
        # 21 call MessageBoxA
        code = (
            b"\x68" + achievementMgrString +   # 0 push offset aCachievementmg
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\xFF"                        # 7 push 0FFFFFFFFh
            b"\x51"                            # 9 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"            # 10 call lua_tolstring
            b"\x83\xC4\x0C"                    # 15 add esp, 0Ch
            b"\x50"                            # 18 push eax
            b"\x6A\x00"                        # 19 push 0
            b"\xFF\x15"                        # 21 call MessageBoxA
        )
        lua_tolstringOffset = 11
        MessageBoxAOffset = 23
        offset = self.exe.codeWildcard(code, b"\xAB")


    if offset is False:
        self.log("Error: CAchievementMgr usage not found")
        exit(1)

    self.lua_tolstringOffset = self.getAddr(offset,
                                            lua_tolstringOffset,
                                            lua_tolstringOffset + 4)

    self.addRawFuncType("lua_tolstring",
                        self.lua_tolstringOffset,
                        "const char *__cdecl lua_tolstring" +
                        "(void *L, int idx, size_t *len);")

    if MessageBoxAOffset != 0:
        self.messageBoxA = self.exe.readUInt(offset + MessageBoxAOffset)
        self.addVaFunc("MessageBoxA", self.messageBoxA)
