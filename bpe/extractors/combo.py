#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCombo(self):
    if self.offset1 is None:
        if self.sendPacket == 0:
            self.comboAddr = 0
            self.log("Packet keys look like not supported. " +
                     "Ignoring combofunction.")
            exit(1)
            return

    if self.sendPacket != 0:
        # 0 MOV ecx, enc_packet_keys
        # 6 PUSH 0
        # 8 CALL combofunction
        # 13 mov ecx, [ebp+buf]
        # 16 xor [ecx], ax
        # 19 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
        code = (
            b"\x8B\x0D\xAB\xAB\xAB\x01" +
            b"\x6A\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB\xAB" +
            b"\x66\x31\xAB" +
            b"\x83\xAB\xAB\xFF")
        encPacketKeysOffset = 2
        comboOffset = 9
        m_socketOffset = 21
        lastSendTimeOffset = 0
        eacOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.sendPacket,
                                       self.sendPacket + 0x200)
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 0
            # 8 CALL combofunction
            # 13 mov ecx, [ebp+buf]
            # 16 xor [ecx], ax
            # 19 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x00" +
                b"\x6A\x00" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x8B\xAB\xAB" +
                b"\x66\x31\xAB" +
                b"\x83\xAB\xAB\xFF")
            encPacketKeysOffset = 2
            comboOffset = 9
            m_socketOffset = 21
            lastSendTimeOffset = 0
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if offset is False:
            # 0 mov ecx, enc_packet_keys
            # 6 push ebx
            # 7 push edi
            # 8 push 0
            # 10 call combofunction
            # 15 mov edi, [ebp+Src]
            # 18 mov ebx, [ebp+Size]
            # 21 xor [edi], ax
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x00" +
                b"\xAB" +
                b"\xAB" +
                b"\x6A\x00" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\xAB\xAB\xAB" +
                b"\xAB\xAB\xAB" +
                b"\x66\x31")
            encPacketKeysOffset = 2
            comboOffset = 11
            m_socketOffset = 0
            lastSendTimeOffset = 0
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if offset is False:
            # 0 mov ecx, enc_packet_keys
            # 6 push ebx
            # 7 push edi
            # 8 push 0
            # 10 call combofunction
            # 15 mov edi, [ebp+Src]
            # 18 mov ebx, [ebp+Size]
            # 21 xor [edi], ax
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x01" +
                b"\xAB" +
                b"\xAB" +
                b"\x6A\x00" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\xAB\xAB\xAB" +
                b"\xAB\xAB\xAB" +
                b"\x66\x31")
            encPacketKeysOffset = 2
            comboOffset = 11
            m_socketOffset = 0
            lastSendTimeOffset = 0
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if offset is False:
            # 2018-06-xx mro
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  call comboFunction
            # 13 mov ebx, [ebp+Src]
            # 16 xor [ebx], ax
            # 19 movzx eax, word ptr [ebx]
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x00"        # 0
                b"\x6A\x00"                        # 6
                b"\xE8\xAB\xAB\xAB\xAB"            # 8
                b"\x8B\x5D\xAB"                    # 13
                b"\x66\x31\x03"                    # 16
                b"\x0F\xB7\x03"                    # 19
            )
            encPacketKeysOffset = 2
            comboOffset = 9
            m_socketOffset = 0
            lastSendTimeOffset = 0
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push ebx
            # 7  push edi
            # 8  push 0
            # 10 call comboFunction
            # 15 mov edi, [ebp+Src]
            # 18 mov ebx, [ebp+Size]
            # 21 push ebx
            # 22 push edi
            # 23 xor [edi], ax
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 0 mov ecx, enc_packet_keys
                b"\x53"                            # 6 push ebx
                b"\x57"                            # 7 push edi
                b"\x6A\x00"                        # 8 push 0
                b"\xE8\xAB\xAB\xAB\xAB"            # 10 call comboFunction
                b"\x8B\x7D\x0C"                    # 15 mov edi, [ebp+Src]
                b"\x8B\x5D\x08"                    # 18 mov ebx, [ebp+Size]
                b"\x53"                            # 21 push ebx
                b"\x57"                            # 22 push edi
                b"\x66\x31\x07"                    # 23 xor [edi], ax
            )
            encPacketKeysOffset = 2
            comboOffset = 11
            m_socketOffset = 0
            lastSendTimeOffset = 0
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)
        if offset is False:
            # 0  call ds:_time32
            # 6  mov ecx, enc_packet_keys
            # 12 add esp, 4
            # 15 mov lastSendTime, eax
            # 20 push 0
            # 22 call comboFunction
            # 27 mov ecx, [ebp+Src]
            # 30 xor [ecx], ax
            code = (
                b"\xFF\x15\xAB\xAB\xAB\xAB"        # 0 call ds:_time32
                b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 6 mov ecx, enc_packet_keys
                b"\x83\xC4\x04"                    # 12 add esp, 4
                b"\xA3\xAB\xAB\xAB\xAB"            # 15 mov lastSendTime, eax
                b"\x6A\x00"                        # 20 push 0
                b"\xE8\xAB\xAB\xAB\xAB"            # 22 call comboFunction
                b"\x8B\x4D\xAB"                    # 27 mov ecx, [ebp+Src]
                b"\x66\x31\x01"                    # 30 xor [ecx], ax
            )
            encPacketKeysOffset = 8
            comboOffset = 23
            m_socketOffset = 0
            lastSendTimeOffset = 16
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)

        if offset is False:
            # 2019-05-29
            # 0  call ds:timeGetTime
            # 6  mov ecx, enc_packet_keys
            # 12 push 0
            # 14 mov lastSendTime, eax
            # 19 call comboFunction
            # 24 mov ecx, [ebp+Src]
            # 27 xor [ecx], ax
            code = (
                b"\xFF\x15\xAB\xAB\xAB\xAB"        # 0 call ds:timeGetTime
                b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 6 mov ecx, enc_packet_keys
                b"\x6A\x00"                        # 12 push 0
                b"\xA3\xAB\xAB\xAB\xAB"            # 14 mov lastSendTime, eax
                b"\xE8\xAB\xAB\xAB\xAB"            # 19 call comboFunction
                b"\x8B\x4D\xAB"                    # 24 mov ecx, [ebp+Src]
                b"\x66\x31\x01"                    # 27 xor [ecx], ax
            )
            encPacketKeysOffset = 8
            comboOffset = 20
            m_socketOffset = 0
            lastSendTimeOffset = 15
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)

        if offset is False:
            # 2019-05-29
            # 0  call ds:timeGetTime
            # 6  mov ecx, enc_packet_keys
            # 12 push 0
            # 14 mov lastSendTime, eax
            # 19 call comboFunction
            # 24 mov edi, [ebp+Src]
            # 27 mov ebx, [ebp+Size]
            # 30 push ebx
            # 31 push edi
            # 32 xor [edi], ax
            code = (
                b"\xFF\x15\xAB\xAB\xAB\xAB"        # 0 call ds:timeGetTime
                b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 6 mov ecx, enc_packet_keys
                b"\x6A\x00"                        # 12 push 0
                b"\xA3\xAB\xAB\xAB\xAB"            # 14 mov lastSendTime, eax
                b"\xE8\xAB\xAB\xAB\xAB"            # 19 call comboFunction
                b"\x8B\x7D\xAB"                    # 24 mov edi, [ebp+Src]
                b"\x8B\x5D\xAB"                    # 27 mov ebx, [ebp+Size]
                b"\x53"                            # 30 push ebx
                b"\x57"                            # 31 push edi
                b"\x66\x31\x07"                    # 32 xor [edi], ax
            )
            encPacketKeysOffset = 8
            comboOffset = 20
            m_socketOffset = 0
            lastSendTimeOffset = 15
            eacOffset = 0
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)

        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  call combofunction
            # 13 mov edi, [ebp+buf]
            # 16 mov ebx, [ebp+Size]
            # 19 xor [edi], ax
            # 22 mov ecx, g_CEasyAntiCheatMgr
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\xAB"       # 0 mov ecx, enc_packet_keys
                b"\x6A\x00"                       # 6 push 0
                b"\xE8\xAB\xAB\xAB\xAB"           # 8 call combofunction
                b"\x8B\x7D\xAB"                   # 13 mov edi, [ebp+buf]
                b"\x8B\x5D\xAB"                   # 16 mov ebx, [ebp+Size]
                b"\x66\x31\x07"                   # 19 xor [edi], ax
                b"\x8B\x0D"                       # 22 mov ecx, g_CEasyAntiChea
            )
            encPacketKeysOffset = 1
            comboOffset = 9
            m_socketOffset = 0
            lastSendTimeOffset = 0
            eacOffset = 24
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x100)

        if self.packetVersion <= "20130814":
            if offset is False:
                # 2013-01-03
                # 0  MOV ecx, enc_packet_keys
                # 6  CALL combofunction
                # 11 mov ecx, [esp+4+buf]
                # 15 xor [ecx], ax
                # 18 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
                code = (
                    b"\x8B\x0D\xAB\xAB\xAB\x00" +
                    b"\xE8\xAB\xAB\xAB\xAB" +
                    b"\x8B\xAB\xAB\xAB" +
                    b"\x66\x31\xAB" +
                    b"\x83\xAB\xAB\xFF")
                encPacketKeysOffset = 2
                comboOffset = 7
                m_socketOffset = 20
                lastSendTimeOffset = 0
                eacOffset = 0
                offset = self.exe.codeWildcard(code,
                                               b"\xAB",
                                               self.sendPacket,
                                               self.sendPacket + 0x200)
                self.comboSimple = True
            if offset is False:
                # 2013-08-14
                # 0  MOV ecx, enc_packet_keys
                # 6  CALL combofunction
                # 11 mov ecx, [ebp+buf]
                # 14 xor [ecx], ax
                # 17 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
                code = (
                    b"\x8B\x0D\xAB\xAB\xAB\x00" +
                    b"\xE8\xAB\xAB\xAB\xAB" +
                    b"\x8B\xAB\xAB" +
                    b"\x66\x31\xAB" +
                    b"\x83\xAB\xAB\xFF")
                encPacketKeysOffset = 2
                comboOffset = 7
                m_socketOffset = 19
                lastSendTimeOffset = 0
                eacOffset = 0
                offset = self.exe.codeWildcard(code,
                                               b"\xAB",
                                               self.sendPacket,
                                               self.sendPacket + 0x200)
                self.comboSimple = True
    if offset is False:
        self.log("Error: failed in search combofunction")
        self.comboAddr = 0
        if (self.clientType == "iro" and self.packetVersion < "20170000") or \
           self.clientType == "euro":
            return
        if self.packetVersion > "20101116":
            exit(1)
        return
    self.comboAddr = self.getAddr(offset, comboOffset, comboOffset + 4)
    self.addRawFunc("comboFunction", self.comboAddr)
    self.encPacketKeys = self.exe.readUInt(offset + encPacketKeysOffset)
    self.addVaVar("enc_packet_keys", self.encPacketKeys)
    if lastSendTimeOffset != 0:
        self.lastSendTime = self.exe.readUInt(offset + lastSendTimeOffset)
        self.addVaVar("lastSendTime", self.lastSendTime)
    if m_socketOffset != 0:
        m_socket = self.exe.readUByte(offset + m_socketOffset)
        self.addStruct("CRagConnection")
        self.addStructMember("m_socket", m_socket, 4, True)
    if eacOffset != 0:
        self.gEasyAntiCheatMgr = self.exe.readUInt(offset + eacOffset)
        self.addVaVar("g_EasyAntiCheatMgr", self.gEasyAntiCheatMgr)

    if self.offset1 is not None:
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 1
            # 8 CALL combofunction
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x00" +
                b"\x6A\x01" +
                b"\xE8\xAB\xAB\xAB\xAB")
            comboOffset = 9
            encPacketKeysOffset = 2
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.offset1 - 0x100,
                                           self.offset1)
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 1
            # 8 CALL combofunction
            code = (
                b"\x8B\x0D\xAB\xAB\xAB\x01" +
                b"\x6A\x01" +
                b"\xE8\xAB\xAB\xAB\xAB")
            comboOffset = 9
            encPacketKeysOffset = 2
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.offset1 - 0x100,
                                           self.offset1)
        if offset is False:
            self.log("Error: failed in search combofunction validation")
            self.comboAddr = 0
            exit(1)
        comboAddr = self.getAddr(offset, comboOffset, comboOffset + 4)
        if self.comboAddr != comboAddr:
            self.log("Error: found different comboFunction")
            self.comboAddr = 0
            exit(1)
        encPacketKeys = self.exe.readUInt(offset + encPacketKeysOffset)
        if encPacketKeys != self.encPacketKeys:
            self.log("Error: found different enc_packet_keys")
            exit(1)
