#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchLoginPacketHandler(self):
    # < 2017-11-XX
    # call WindowFunc
    # mov ecx, esi
    # call CLoginMode::PollNetworkStatus
    # mov ecx, esi
    # call CLoginMode::func
    # mov ecx, offset addr1
    # call func1
    # mov ecx, g_windowMgr
    # call UIWindowMgr::func2
    # mov ecx, esi
    # call addr3
    # cmp [esi + N], 0
    # jz addr4
    code = (
        b"\xE8\xAB\xAB\xAB\x00" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\x00\x00" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\xB9\xAB\xAB\xAB\x00" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\xB9\xAB\xAB\xAB\x00" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x83\xAB\xAB\x00" +
        b"\x74\xAB")
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2003-10-28
        # call WindowFunc
        # mov ecx, esi
        # call CLoginMode::PollNetworkStatus
        # mov eax, A
        # test eax, eax
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x00\x00" +
            b"\xA1\xAB\xAB\xAB\x00" +
            b"\x85\xAB" +
            b"\x74\xAB")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2004-06-21
        # call WindowFunc
        # mov ecx, ebx
        # call CLoginMode::PollNetworkStatus
        # mov eax, [ebx + A]
        # cmp eax, B
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x00\x00" +
            b"\x8B\xAB\xAB" +
            b"\x83\xAB\xAB" +
            b"\x74\xAB")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-11-04
        # call WindowFunc
        # mov ecx, esi
        # call CLoginMode::PollNetworkStatus
        # mov ecx, esi
        # call CLoginMode::func
        # mov ecx, offset addr1
        # call func1
        # mov ecx, g_windowMgr
        # call UIWindowMgr::func2
        # mov ecx, esi
        # call addr3
        # cmp [esi + N], 0
        # jz addr4
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xFF\xFF" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x83\xAB\xAB\x00" +
            b"\x74\xAB")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in search CLoginMode::PollNetworkStatus")
        exit(1)
        return
    self.windowFunc = self.getAddr(offset, 1, 5)
    self.loginPollAddr = self.getAddr(offset, 8, 12)
    self.loginPollAddrVa = self.exe.rawToVa(self.loginPollAddr)
    self.addRawFunc("WindowFunc", self.windowFunc)
    self.addRawFunc("CLoginMode::PollNetworkStatus",
                    self.loginPollAddr)
