#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSendPacket(self, errorExit):
    # 1. search for call pattern
    # 2017-11-XX
    # push 2
    # call CRagConnection::instanceR
    # mov ecx, eax
    # call CRagConnection::SendPacket
    instanceOffset = 3
    sendOffset = 10
    code = (
        b"\x6A\x02" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xAB" +
        b"\xE8")
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.gamePollAddr,
                                   self.gamePollAddr + 0x10000)
    if offset is False:
        # 2011-01-04
        # push 2
        # mov word ptr [esp + N + M], cx
        # call CRagConnection::instanceR
        # mov ecx, eax
        # call CRagConnection::SendPacket
        code = (
            b"\x6A\x02" +
            b"\x66\xAB\xAB\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB" +
            b"\xE8")
        instanceOffset = 8
        sendOffset = 15
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x10000)
    if offset is False:
        # 2013-01-15
        # push 2
        # mov word ptr [ebp + N], dx
        # call CRagConnection::instanceR
        # mov ecx, eax
        # call CRagConnection::SendPacket
        code = (
            b"\x6A\x02" +
            b"\x66\xAB\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8B\xAB" +
            b"\xE8")
        instanceOffset = 7
        sendOffset = 14
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x10000)
    if offset is False:
        self.log("failed searchSendPacket in step 1.")
        if errorExit is True:
            exit(1)
        return False
    # 2. compare is first call is really CRagConnection::instanceR
    if self.instanceR != 0:
        if self.getAddr(offset,
                        instanceOffset,
                        instanceOffset + 4
                        ) != self.instanceR:
            self.log("failed searchSendPacket in step 2.")
            if errorExit is not False:
                exit(1)
            return False
    sendPacket = self.getAddr(offset, sendOffset, sendOffset + 4)
    if self.sendPacket != 0:
        if sendPacket != self.sendPacket:
            self.log("Error: CRagConnection::SendPacket validation failed")
            exit(1)
    else:
        self.sendPacket = sendPacket
        self.sendPacketVa = self.exe.rawToVa(self.sendPacket)
        self.addRawFunc("CRagConnection::SendPacket",
                        self.sendPacket)
    return True
