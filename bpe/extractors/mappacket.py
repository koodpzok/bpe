#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchMapPacketHandler(self):
    # < 2017-11-XX
    # call WindowFunc
    # mov ecx, esi
    # call CGameMode::PollNetworkStatus
    # call addr1
    # mov ecx, eax
    # call addr2
    code = (
        b"\xE8\xAB\xAB\xAB\x00" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\xAB\x00" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x85\xAB")
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2003-10-28
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # mov eax, [esi + A]
        # mov ebx, ds:timeGetTime
        # test eax, eax
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x01\x00" +
            b"\x8B\xAB\xAB" +
            b"\x8B\xAB\xAB\xAB\xAB\x00" +
            b"\x85\xAB" +
            b"\x0F\x84\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2004-02-23
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # mov eax, [esi + A]
        # test eax, eax
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x01\x00" +
            b"\x8B\xAB\xAB" +
            b"\x85\xAB" +
            b"\x0F\x84\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2007-03-12
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # mov eax, [esi + A]
        # test eax, eax
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x02\x00" +
            b"\x8B\xAB\xAB" +
            b"\x85\xAB" +
            b"\x0F\x84\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2007-10-23
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # lea ecx, [esi + A]
        # call addr1
        # mov eax, [esi + B]
        # test eax, eax
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8D\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8B\xAB\xAB" +
            b"\x85\xAB" +
            b"\x0F\x84\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2008-05-20
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # lea ecx, [esi + A]
        # call addr1
        # cmp dword ptr [esi + A], 0
        # jz addr
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x03\x00" +
            b"\x8D\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x83\xAB\xAB\x00" +
            b"\x0F\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2008-09-03
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # mov eax, [esi + A]
        # test eax, eax
        # jz addr
        # call addr1
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB\xAB" +
            b"\x85\xC0" +
            b"\x0F\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-01-04
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # cmp dword ptr [esi + A], 0
        # jz addr
        # call addr2
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x83\xAB\xAB\x00" +
            b"\x0F\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\xAB\xFF")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-01-04
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # call addr1
        # cmp eax, 2
        # jnz addr2
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x03\x00" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x83\xAB\xAB" +
            b"\x75\xAB")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-03-29
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # call addr1
        # mov ecx, eax
        # call addr2
        # test eax, eax
        # jnz addr3
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\x03\x00" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x85\xAB" +
            b"\x75\xAB")
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  call WindowFunc
        # 5  mov ecx, esi
        # 7  call sub_887690
        # 12 cmp dword ptr [esi+14h], 0
        # 16 jz loc_86935D
        # 22 mov ecx, dword_C490E4
        code = (
            b"\xE8\xAB\xAB\xAB\x00"            # 0
            b"\x8B\xCE"                        # 5
            b"\xE8\xAB\xAB\xAB\x00"            # 7
            b"\x83\x7E\xAB\x00"                # 12
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 16
            b"\x8B\x0D"                        # 22
        )
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in search CGameMode::PollNetworkStatus")
        exit(1)
        return
    if self.windowFunc != self.getAddr(offset, 1, 5):
        self.log("failed in validation CLoginMode::PollNetworkStatus" +
                 ", CGameMode::PollNetworkStatus")
        exit(1)
        return
    self.gamePollAddr = self.getAddr(offset, 8, 12)
    self.gamePollAddrVa = self.exe.rawToVa(self.gamePollAddr)
    self.addRawFunc("CGameMode::PollNetworkStatus",
                    self.gamePollAddr)
