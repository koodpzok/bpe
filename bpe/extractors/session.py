#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSession(self, errorExit):
    if self.getGameModeAddrVa == 0 or self.g_modeMgr == 0:
        self.log("Error: skip search g_session")
        exit(1)
    # search for some CSession functions

    # 0  mov ecx, offset g_modeMgr
    # 5  call CModeMgr::GetGameMode
    # 10 mov eax, [eax+m_world]
    # 16 mov eax, [eax+offset2]
    # 19 retn
    getGameModeOffset = 6
    mWorldOffset = 12
    offset2 = 18
    code = (
        b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\x80\xAB\x00\x00\x00" +
        b"\x8B\x40\xAB" +
        b"\xC3")
    offsets = self.exe.codesWildcard(code, b"\xAB", -1)
    offsets2 = []
    if offsets is False:
        self.log("Error: failed in seach g_session functions")
        if errorExit is True and self.packetVersion > "20090000":
            exit(1)
        return
    sz = len(offsets)
    if sz == 1 and self.packetVersion < "20080000":
        pass
    elif sz != 2:
        self.log("Error: found wrong number of g_session functions: {0}".
                 format(sz))
        if errorExit is True and self.packetVersion > "20050000":
            exit(1)
        return

    for offset in offsets:
        getGameMode = self.getAddr(offset,
                                   getGameModeOffset,
                                   getGameModeOffset + 4)
        if getGameMode != self.getGameModeAddr:
            self.log("Error: found wrong CModeMgr::GetGameMode "
                     "in g_session calls.")
            exit(1)
            return
        mWorld = self.exe.readUInt(offset + mWorldOffset)
        if self.CGameMode_mWorldOffset != 0:
            if self.CGameMode_mWorldOffset != mWorld:
                self.log("Error: found different m_world offsets ")
                exit(1)
                return
        else:
            self.CGameMode_mWorldOffset = mWorld
        offsets2.append(self.exe.read(offset + offset2, 1, 'B'))
    if sz == 1:
        idx1 = 0
        idx2 = -1
    elif offsets2[0] < offsets2[1]:
        idx1 = 0
        idx2 = 1
    else:
        idx1 = 1
        idx2 = 0
    self.CSession_IsSiegeMode = offsets[idx1]
    self.addRawFunc("CSession::IsSiegeMode", self.CSession_IsSiegeMode)
    if idx2 != -1:
        self.CSession_IsBattleFieldMode = offsets[idx2]
        self.addRawFunc("CSession::IsBattleFieldMode",
                        self.CSession_IsBattleFieldMode)
    self.CWorld_IsSiegeModeOffset = offsets2[idx1]
    self.addStruct("CWorld")
    self.addStructMember("IsSiegeMode", self.CWorld_IsSiegeModeOffset, 4, True)
    if idx2 != -1:
        self.CWorld_IsBattleFieldModeOffset = offsets2[idx2]
        self.addStructMember("IsBattleFieldMode",
                             self.CWorld_IsBattleFieldModeOffset,
                             4,
                             True)
    self.addStruct("CGameMode")
    self.addStructMember("m_world", self.CGameMode_mWorldOffset, 4, True)
    self.setStructMemberType(self.CGameMode_mWorldOffset, "CWorld*")

    # search for g_session

    # 0  mov ecx, offset g_session
    # 5  call CSession::IsSiegeMode
    sessionOffset = 1
    isSiegeModeOffset = 6
    code = (
        b"\xB9\xAB\xAB\xAB\xAB" +
        b"\xE8")
    offsets = self.exe.codesWildcard(code, b"\xAB", -1)
    if offsets is False:
        self.log("Error: failed in seach g_session")
        exit(1)
        return
    offsets2 = []
    session = 0
    for offset in offsets:
        errors = 0
        isSiegeMode = self.getAddr(offset,
                                   isSiegeModeOffset,
                                   isSiegeModeOffset + 4)
        if isSiegeMode == self.CSession_IsSiegeMode:
            session2 = self.exe.readUInt(offset + sessionOffset)
            if session != 0:
                if session != session2:
                    if errors < 1:
                        errors = errors + 1
                    else:
                        self.log("Error: found different g_session")
                        if errorExit is True and \
                           self.packetVersion < "20080000":
                            exit(1)
                        return
            else:
                session = session2
    if session == 0:
        self.log("Error: g_session not found")
        exit(1)
        return

    self.session = session
    self.addVaVar("g_session", self.session)
