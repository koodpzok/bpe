#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# seaech inside CConnection::Connect


def searchConnect(self):
    offset, section = self.exe.string(b"Failed to setup select mode")
    if offset is False:
        self.log("Error: 'Failed to setup select mode' not found")
        if self.packetVersion < "20040000":
            return
        exit(1)
    strHex = self.exe.toHex(section.rawToVa(offset), 4)

    # 2017-11-01
    # 0  push offset aFailedToSetupS
    # 5  call ErrorMsg
    # 10 add esp, 4
    code = (
        b"\x68" + strHex +                 # 0 push offset aFailedToSetupS
        b"\xE8\xAB\xAB\xAB\xAB"            # 5 call ErrorMsg
        b"\x83\xC4\x04"                    # 10 add esp, 4
    )
    errorMsgOffset = 6
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2019-06-05
        # 0  push 0
        # 2  push offset aFailedToSetupS
        # 7  call ErrorMsg
        # 12 add esp, 8
        code = (
            b"\x6A\x00"                        # 0 push 0
            b"\x68" + strHex +                 # 2 push offset aFailedToSetupS
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call ErrorMsg
            b"\x83\xC4\x08"                    # 12 add esp, 8
        )
        errorMsgOffset = 8
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: 'Failed to setup select mode' usage not found")
        exit(1)

    self.errorMsg = self.getAddr(offset, errorMsgOffset, errorMsgOffset + 4)
    self.addRawFunc("ErrorMsg", self.errorMsg)

    messageOffset = offset

    # 2017-11-01
    # 0  mov [ebx+CConnection.m_socket_addr.sin_port], ax
    # 4  push 10h
    # 6  lea eax, [ebx+CConnection.m_socket_addr]
    # 9  push eax
    # 10 push [ebx+CConnection.m_socket]
    # 13 call connect
    # 19 cmp eax, 0FFFFFFFFh
    # 22 jnz short loc_94D16F
    # 24 mov esi, WSAGetLastError
    # 30 call esi ; WSAGetLastError
    # 32 cmp eax, 2733h
    # 37 jz short loc_94D16F
    # 39 call esi ; WSAGetLastError
    # 41 mov ecx, ebx
    # 43 call CConnection_Disconnect
    # 48 jmp short loc_94D199
    code = (
        b"\x66\x89\x43\xAB"                # 0 mov [ebx+CConnection.m_socket_ad
        b"\x6A\x10"                        # 4 push 10h
        b"\x8D\x43\xAB"                    # 6 lea eax, [ebx+CConnection.m_sock
        b"\x50"                            # 9 push eax
        b"\xFF\x73\xAB"                    # 10 push [ebx+CConnection.m_socket]
        b"\xFF\x15\xAB\xAB\xAB\xAB"        # 13 call connect
        b"\x83\xF8\xFF"                    # 19 cmp eax, 0FFFFFFFFh
        b"\x75\x1A"                        # 22 jnz short loc_94D16F
        b"\x8B\x35\xAB\xAB\xAB\xAB"        # 24 mov esi, WSAGetLastError
        b"\xFF\xD6"                        # 30 call esi ; WSAGetLastError
        b"\x3D\x33\x27\x00\x00"            # 32 cmp eax, 2733h
        b"\x74\x0B"                        # 37 jz short loc_94D16F
        b"\xFF\xD6"                        # 39 call esi ; WSAGetLastError
        b"\x8B\xCB"                        # 41 mov ecx, ebx
        b"\xE8\xAB\xAB\xAB\xAB"            # 43 call CConnection_Disconnect
        b"\xEB"                            # 48 jmp short loc_94D199
    )
    sinPortOffset = (3, 1)
    socketAddrOffset = (8, 1)
    socketOffset = (12, 1)
    connectOffset = (15, True)
    connectCallOffset1 = 13
    connectCallOffset2 = 19
    wsaErrorOffset = (26, True)
    wsaErrorOffset2 = 0
    disconnectOffset = (44, False)
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   messageOffset - 0x50,
                                   messageOffset)

    if offset is False:
        # 2019-06-05
        # 0  push 10h
        # 2  lea eax, [edi+CConnection.m_socket_addr]
        # 5  mov [edi+CConnection.m_socket_addr.sin_port], si
        # 9  push eax
        # 10 push [edi+CConnection.m_socket]
        # 13 call ds:connect
        # 19 cmp eax, 0FFFFFFFFh
        # 22 jnz short loc_97E101
        # 24 mov esi, ds:WSAGetLastError
        # 30 call esi ; WSAGetLastError
        # 32 cmp eax, 2733h
        # 37 jz short loc_97E101
        # 39 call esi ; WSAGetLastError
        # 41 mov ecx, edi
        # 43 call CConnection_Disconnect
        # 48 jmp short loc_97E11B
        code = (
            b"\x6A\x10"                        # 0 push 10h
            b"\x8D\x47\xAB"                    # 2 lea eax, [edi+CConnection.m_
            b"\x66\x89\x77\xAB"                # 5 mov [edi+CConnection.m_socke
            b"\x50"                            # 9 push eax
            b"\xFF\x77\xAB"                    # 10 push [edi+CConnection.m_soc
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 13 call ds:connect
            b"\x83\xF8\xFF"                    # 19 cmp eax, 0FFFFFFFFh
            b"\x75\x1A"                        # 22 jnz short loc_97E101
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 24 mov esi, ds:WSAGetLastError
            b"\xFF\xD6"                        # 30 call esi ; WSAGetLastError
            b"\x3D\x33\x27\x00\x00"            # 32 cmp eax, 2733h
            b"\x74\x0B"                        # 37 jz short loc_97E101
            b"\xFF\xD6"                        # 39 call esi ; WSAGetLastError
            b"\x8B\xCF"                        # 41 mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 43 call CConnection_Disconnect
            b"\xEB"                            # 48 jmp short loc_97E11B
        )
        sinPortOffset = (8, 1)
        socketAddrOffset = (4, 1)
        socketOffset = (12, 1)
        connectOffset = (15, True)
        connectCallOffset1 = 13
        connectCallOffset2 = 19
        wsaErrorOffset = (26, True)
        wsaErrorOffset2 = 0
        disconnectOffset = (44, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0x50,
                                       messageOffset)

    if offset is False:
        # 2017-01-04
        # 0  mov [ebx+CConnection.m_socket_addr.sin_port], ax
        # 4  push 10h
        # 6  push edi
        # 7  push [ebx+CConnection.m_socket]
        # 10 call connect
        # 16 cmp eax, 0FFFFFFFFh
        # 19 jnz short loc_8E3142
        # 21 mov esi, WSAGetLastError
        # 27 call esi
        # 29 cmp eax, 2733h
        # 34 jz short loc_8E3142
        # 36 call esi
        # 38 mov ecx, ebx
        # 40 call CConnection_Disconnect
        code = (
            b"\x66\x89\x43\xAB"                # 0 mov [ebx+CConnection.m_socke
            b"\x6A\x10"                        # 4 push 10h
            b"\x57"                            # 6 push edi
            b"\xFF\x73\xAB"                    # 7 push [ebx+CConnection.m_sock
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 10 call connect
            b"\x83\xF8\xFF"                    # 16 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 19 jnz short loc_8E3142
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 21 mov esi, WSAGetLastError
            b"\xFF\xD6"                        # 27 call esi
            b"\x3D\x33\x27\x00\x00"            # 29 cmp eax, 2733h
            b"\x74\xAB"                        # 34 jz short loc_8E3142
            b"\xFF\xD6"                        # 36 call esi
            b"\x8B\xCB"                        # 38 mov ecx, ebx
            b"\xE8"                            # 40 call CConnection_Disconnect
        )
        sinPortOffset = (3, 1)
        socketAddrOffset = 0
        socketOffset = (9, 1)
        connectOffset = (12, True)
        connectCallOffset1 = 10
        connectCallOffset2 = 16
        wsaErrorOffset = (23, True)
        wsaErrorOffset2 = 0
        disconnectOffset = (41, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0x50,
                                       messageOffset)

    if offset is False:
        # 2016-04-27
        # 0  mov [ebx+CConnection.m_socket_addr.sin_port], ax
        # 4  push 10h
        # 6  push edi
        # 7  push [ebx+CConnection.m_socket]
        # 10 call near ptr 71A94A07h
        # 15 no nop
        # 16 cmp eax, 0FFFFFFFFh
        # 19 jnz short loc_8C3C22
        # 21 mov esi, WSAGetLastError
        # 27 call esi
        # 29 cmp eax, 2733h
        # 34 jz short loc_8C3C22
        # 36 call esi
        # 38 mov ecx, ebx
        # 40 call CConnection_Disconnect
        code = (
            b"\x66\x89\x43\xAB"                # 0 mov [ebx+CConnection.m_socke
            b"\x6A\x10"                        # 4 push 10h
            b"\x57"                            # 6 push edi
            b"\xFF\x73\xAB"                    # 7 push [ebx+CConnection.m_sock
            b"\xE8\xAB\xAB\xAB\xAB"            # 10 call near ptr 71A94A07h
            b"\x90"                            # 15 no nop
            b"\x83\xF8\xFF"                    # 16 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 19 jnz short loc_8C3C22
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 21 mov esi, WSAGetLastError
            b"\xFF\xD6"                        # 27 call esi
            b"\x3D\x33\x27\x00\x00"            # 29 cmp eax, 2733h
            b"\x74\xAB"                        # 34 jz short loc_8C3C22
            b"\xFF\xD6"                        # 36 call esi
            b"\x8B\xCB"                        # 38 mov ecx, ebx
            b"\xE8"                            # 40 call CConnection_Disconnect
        )
        sinPortOffset = (3, 1)
        socketAddrOffset = 0
        socketOffset = (9, 1)
        connectOffset = (11, False)
        connectCallOffset1 = 0
        connectCallOffset2 = 0
        wsaErrorOffset = (23, True)
        wsaErrorOffset2 = 0
        disconnectOffset = (41, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0x50,
                                       messageOffset)

    if offset is False:
        # 2015-01-07
        # 0  mov ecx, [esi+CConnection.m_socket]
        # 3  push 10h
        # 5  push edi
        # 6  push ecx
        # 7  mov [esi+CConnection.m_socket_addr.sin_port], ax
        # 11 call connect
        # 17 cmp eax, 0FFFFFFFFh
        # 20 jnz short loc_7B4F34
        # 22 mov edi, WSAGetLastError
        # 28 call edi
        # 30 cmp eax, 2733h
        # 35 jz short loc_7B4F34
        # 37 call edi
        # 39 mov ecx, esi
        # 41 call CConnection_Disconnect
        code = (
            b"\x8B\x4E\xAB"                    # 0 mov ecx, [esi+CConnection.m_
            b"\x6A\x10"                        # 3 push 10h
            b"\x57"                            # 5 push edi
            b"\x51"                            # 6 push ecx
            b"\x66\x89\x46\xAB"                # 7 mov [esi+CConnection.m_socke
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 11 call connect
            b"\x83\xF8\xFF"                    # 17 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 20 jnz short loc_7B4F34
            b"\x8B\x3D\xAB\xAB\xAB\xAB"        # 22 mov edi, WSAGetLastError
            b"\xFF\xD7"                        # 28 call edi
            b"\x3D\x33\x27\x00\x00"            # 30 cmp eax, 2733h
            b"\x74\xAB"                        # 35 jz short loc_7B4F34
            b"\xFF\xD7"                        # 37 call edi
            b"\x8B\xCE"                        # 39 mov ecx, esi
            b"\xE8"                            # 41 call CConnection_Disconnect
        )
        sinPortOffset = (10, 1)
        socketAddrOffset = 0
        socketOffset = (2, 1)
        connectOffset = (13, True)
        connectCallOffset1 = 11
        connectCallOffset2 = 17
        wsaErrorOffset = (24, True)
        wsaErrorOffset2 = 0
        disconnectOffset = (42, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0x50,
                                       messageOffset)

    if offset is False:
        # 2015-07-22
        # 0  mov [ebx+CConnection.m_socket_addr.sin_port], ax
        # 4  push 10h
        # 6  push edi
        # 7  push [ebx+CConnection.m_socket]
        # 10 no nop
        # 11 call near ptr 71A94A07h
        # 16 cmp eax, 0FFFFFFFFh
        # 19 jnz short loc_7F4BD2
        # 21 mov esi, WSAGetLastError
        # 27 call esi
        # 29 cmp eax, 2733h
        # 34 jz short loc_7F4BD2
        # 36 call esi
        # 38 mov ecx, ebx
        # 40 call CConnection_Disconnect
        code = (
            b"\x66\x89\x43\xAB"                # 0 mov [ebx+CConnection.m_socke
            b"\x6A\x10"                        # 4 push 10h
            b"\x57"                            # 6 push edi
            b"\xFF\x73\xAB"                    # 7 push [ebx+CConnection.m_sock
            b"\x90"                            # 10 no nop
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call near ptr 71A94A07h
            b"\x83\xF8\xFF"                    # 16 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 19 jnz short loc_7F4BD2
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 21 mov esi, WSAGetLastError
            b"\xFF\xD6"                        # 27 call esi
            b"\x3D\x33\x27\x00\x00"            # 29 cmp eax, 2733h
            b"\x74\xAB"                        # 34 jz short loc_7F4BD2
            b"\xFF\xD6"                        # 36 call esi
            b"\x8B\xCB"                        # 38 mov ecx, ebx
            b"\xE8"                            # 40 call CConnection_Disconnect
        )
        sinPortOffset = (3, 1)
        socketAddrOffset = 0
        socketOffset = (9, 1)
        connectOffset = (12, False)
        connectCallOffset1 = 0
        connectCallOffset2 = 0
        wsaErrorOffset = (23, True)
        wsaErrorOffset2 = 0
        disconnectOffset = (41, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0x50,
                                       messageOffset)

    if offset is False:
        # 2010-01-05
        # 0  mov ecx, [esi+CConnection.m_socket]
        # 3  push 10h
        # 5  push ebx
        # 6  push ecx
        # 7  mov [esi+CConnection.m_socket_addr.sin_port], ax
        # 11 call connect
        # 16 cmp eax, 0FFFFFFFFh
        # 19 jnz short loc_41876F
        # 21 call WSAGetLastError
        # 26 cmp eax, 2733h
        # 31 jz short loc_41876F
        # 33 call WSAGetLastError
        code = (
            b"\x8B\x4E\xAB"                    # 0 mov ecx, [esi+CConnection.m_
            b"\x6A\x10"                        # 3 push 10h
            b"\x53"                            # 5 push ebx
            b"\x51"                            # 6 push ecx
            b"\x66\x89\x46\xAB"                # 7 mov [esi+CConnection.m_socke
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call connect
            b"\x83\xF8\xFF"                    # 16 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 19 jnz short loc_41876F
            b"\xE8\xAB\xAB\xAB\xAB"            # 21 call WSAGetLastError
            b"\x3D\x33\x27\x00\x00"            # 26 cmp eax, 2733h
            b"\x74\xAB"                        # 31 jz short loc_41876F
            b"\xE8"                            # 33 call WSAGetLastError
        )
        sinPortOffset = (10, 1)
        socketAddrOffset = 0
        socketOffset = (2, 1)
        connectOffset = (12, False)
        connectCallOffset1 = 0
        connectCallOffset2 = 0
        wsaErrorOffset = (22, False)
        wsaErrorOffset2 = (34, False)
        disconnectOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0xa0,
                                       messageOffset)

    if offset is False:
        # 2010-08-17
        # 0  mov ecx, [esi+CConnection.m_socket]
        # 3  push 10h
        # 5  push edi
        # 6  push ecx
        # 7  mov [esi+CConnection.m_socket_addr.sin_port], ax
        # 11 call connect
        # 16 pop edi
        # 17 pop ebx
        # 18 cmp eax, 0FFFFFFFFh
        # 21 jnz short loc_42387D
        # 23 call WSAGetLastError
        # 28 cmp eax, 2733h
        # 33 jz short loc_42387D
        # 35 call WSAGetLastError
        # 40 mov ecx, esi
        # 42 call CConnection_Disconnect
        code = (
            b"\x8B\x4E\xAB"                    # 0 mov ecx, [esi+CConnection.m_
            b"\x6A\x10"                        # 3 push 10h
            b"\x57"                            # 5 push edi
            b"\x51"                            # 6 push ecx
            b"\x66\x89\x46\xAB"                # 7 mov [esi+CConnection.m_socke
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call connect
            b"\x5F"                            # 16 pop edi
            b"\x5B"                            # 17 pop ebx
            b"\x83\xF8\xFF"                    # 18 cmp eax, 0FFFFFFFFh
            b"\x75\xAB"                        # 21 jnz short loc_42387D
            b"\xE8\xAB\xAB\xAB\xAB"            # 23 call WSAGetLastError
            b"\x3D\x33\x27\x00\x00"            # 28 cmp eax, 2733h
            b"\x74\xAB"                        # 33 jz short loc_42387D
            b"\xE8\xAB\xAB\xAB\xAB"            # 35 call WSAGetLastError
            b"\x8B\xCE"                        # 40 mov ecx, esi
            b"\xE8"                            # 42 call CConnection_Disconnect
        )
        sinPortOffset = (10, 1)
        socketAddrOffset = 0
        socketOffset = (2, 1)
        connectOffset = (12, False)
        connectCallOffset1 = 0
        connectCallOffset2 = 0
        wsaErrorOffset = (24, False)
        wsaErrorOffset2 = (36, False)
        disconnectOffset = (43, False)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       messageOffset - 0xa0,
                                       messageOffset)


    if offset is False:
        self.log("Error: connect not found")
        exit(1)

    self.connect = self.exe.getFuncAddrVa(offset, connectOffset)
    self.addVaFunc("connect", self.connect)
    if connectCallOffset2 == 0:
        self.connectCall1 = 0
        self.connectCall2 = 0
    else:
        self.connectCall1 = self.exe.rawToVa(offset + connectCallOffset1)
        self.connectCall2 = self.exe.rawToVa(offset + connectCallOffset2)

    sinPort = self.getVarAddr(offset, sinPortOffset)
    if socketAddrOffset != 0:
        socketAddr = self.getVarAddr(offset, socketAddrOffset)
        if socketAddr + 2 != sinPort:
            self.log("Error: CConnection.m_socket_addr is wrong")
            exit(1)
    else:
        socketAddr = sinPort - 2
    socket = self.getVarAddr(offset, socketOffset)

    self.addStruct("CConnection")
    self.addStructMember("m_socket", socket, 4, True)
    self.addStructMember("m_socket_addr", socketAddr, 4, True)

    self.WSAGetLastError = self.exe.getFuncAddrVa(offset, wsaErrorOffset)
    self.addVaFunc("WSAGetLastError", self.WSAGetLastError)
    if wsaErrorOffset2 != 0:
        self.WSAGetLastError = self.exe.getFuncAddrVa(offset, wsaErrorOffset2)
        self.addVaFunc("WSAGetLastError", self.WSAGetLastError)

    if disconnectOffset != 0:
        self.CConnection_Disconnect = self.exe.getFuncAddrVa(offset,
                                                             disconnectOffset)
        self.addVaFunc("CConnection::Disconnect", self.CConnection_Disconnect)
