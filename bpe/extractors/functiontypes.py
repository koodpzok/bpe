#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def setFunctionTypes(self):
    # ITEM_INFO::init
    if self.ITEM_INFO_init != 0:
        self.setRawFuncType(self.ITEM_INFO_init,
                            "void __thiscall ITEM_INFO_init("
                            "ITEM_INFO *itemInfo)")
    # ITEM_INFO::SetItemId
    if self.ITEM_INFO_SetItemId != 0:
        self.setRawFuncType(self.ITEM_INFO_SetItemId,
                            "void __thiscall ITEM_INFO_SetItemId"
                            "(ITEM_INFO *this, int itemId)")
    # ITEM_INFO::GetIdDisplayName
    if self.ITEM_INFO_GetIdDisplayName != 0:
        self.setRawFuncType(self.ITEM_INFO_GetIdDisplayName,
                            "int __thiscall ITEM_INFO_GetIdDisplayName"
                            "(ITEM_INFO *this, char *dstName)")
