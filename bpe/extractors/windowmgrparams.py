#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.params import searchParams2


def searchWindowMgrParams(self, errorExit):
    vals = (
        (b"m_minimapZoomWnd", "m_miniMapZoomFactor", 4),
        (b"M_ISDRAGALL", "m_isDragAll", 4),
        (b"M_ISDRAWCOMPASS", "m_isDrawCompass", 4),
        (b"M_MINIMAPARGB", "m_miniMapArgb", 4),
        (b"M_CHATWNDSTATUS", "m_chatWndStatus", 4),
        (b"M_CHATWNDHEIGHT", "m_chatWndHeight", 4),
        (b"M_CHATWNDX", "m_chatWndX", 4),
        (b"M_CHATWNDY", "m_chatWndY", 4),
        (b"M_CHATWNDSHOW", "m_chatWndShow", 4),
        (b"CHATWNDSTICKON", "m_chatWndStickOn", 1),
        (b"ONSTSUBCHAT", "m_onStSubChat", 1),
        (b"ONBTSUBCHAT", "m_onBtSubChat", 1),
        (b"ITEMSTOREWNDNUM", "m_ITEMSTOREWNDNUM", 4),
    )
    from bpe.blocks.windowmgrparams import blocks
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           True,
                           4)
    self.addStruct("UIWindowMgr")
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1], arr[val0], val[2], True)

    props = (
        (b"X", "x"),
        (b"Y", "y"),
        (b"W", "w"),
        (b"H", "h"),
    )
    vals = (
        (b"SAYDIALOGWNDINFO", "sayDialogWndInfo"),
        (b"CHOOSEWNDINFO", "chooseWndInfo"),
        (b"BASICINFOWNDINFO", "basicInfoWndInfo"),
        (b"ITEMWNDINFO", "itemWndInfo"),
        (b"QUESTWNDINFO", "questWndInfo"),
        (b"STATUSWNDINFO", "statusWndInfo"),
        (b"EQUIPWNDINFO", "equipWndInfo"),
        (b"OPTIONWNDINFO", "optionWndInfo"),
        (b"SHORTENITEMWNDINFO", "shortenItemWndInfo"),
        (b"SHORTCUTWNDINFO", "shortCutWndInfo"),
        (b"CHATROOMWNDINFO", "chatRoomWndInfo"),
        (b"ITEMSTOREFINDWNDINFO", "itemStoreFindWndInfo"),
        (b"ITEMSHOPWNDINFO", "itemShopWndInfo"),
        (b"ITEMSELLWNDINFO", "itemSellWndInfo"),
        (b"ITEMPURCHASEWNDINFO", "itemPurchaseWndInfo"),
        (b"MESSENGERGROUPWNDINFO", "messengerGroupWndInfo"),
        (b"EXCHANGEACCEPTWNDINFO", "exchangeAcceptWndInfo"),
        (b"SKILLLISTWNDINFO", "skillListWndInfo"),
        (b"MERCHANTITEMWNDINFO", "merchantItemWndInfo"),
        (b"MERCHANTMIRRORITEMWNDINFO", "merchantMirrorItemWndInfo"),
        (b"MERCHANTSHOPMAKEWNDINFO", "merchantShopMakeWndInfo"),
        (b"MERCHANTITEMSHOPWNDINFO", "merchantItemShopWndInfo"),
        (b"MERCHANTITEMMYSHOPWNDINFO", "merchantItemMyShopWndInfo"),
        (b"MERCHANTITEMPURCHASEWNDINFO", "merchantItemPurchaseWndInfo"),
        (b"ITEMCOLLECTIONWNDINFO", "itemCollectionWndInfo"),
        (b"COMBINEDCARDITEMCOLLECTIONWNDINFO",
         "combinedCardItemCollectionWndInfo"),
        (b"ITEMPARAMCHANGEDISPLAYWNDINFO", "itemParamChangeDisplayWndInfo"),
        (b"PARTYSETTINGWNDINFO", "partySettingWndInfo"),
        (b"DETAILLEVELWNDINFO", "detailLevelWndInfo"),
        (b"WHISPERWNDINFO", "whisperWndInfo"),
        (b"FRIENDOPTIONWNDINFO", "friendOptionWndInfo"),
        (b"ITEMCOMPOSITIONWNDINFO", "itemCompositionWndInfo"),
        (b"ITEMIDENTIFYWNDINFO", "itemIdentifyWndInfo"),
        (b"SUBCHATWNDST", "subChatWndStInfo"),
        (b"SUBCHATWNDBT", "subChatWndBtInfo"),
        (b"BOOKINGLISTWNDINFO", "bookingListWndInfo"),
        (b"BOOKINGMBWNDINFO", "bookingMbWndInfo"),
        (b"BOOKINGWNDINFO", "bookingWndInfo"),
        (b"BOOKINGMBCHECKWNDINFO", "bookingMbCheckWndInfo"),
        (b"QUICKSLOTWNDINFO", "quickSlotWndInfo"),
        (b"BATTLEFIELDWNDINFO", "battleFieldWndInfo"),
        (b"GUILDHELPERWNDINFO", "guildHelperWndInfo"),
        (b"BUYINGSTOREMAKEWNDINFO", "buyingStoreMakeWndInfo"),
        (b"BUYINGSTOREMIRRORITEMWNDINFO", "buyingStoreMirrorItemWndInfo"),
        (b"BUYINGSTOREITEMMYSHOPWNDINFO", "buyingStoreItemMyShopWndInfo"),
        (b"BUYINGSTOREITEMSHOPWNDINFO", "buyingStoreItemShopWndInfo"),
        (b"BUYINGSTOREITEMSELLWNDINFO", "buyingStoreItemSellWndInfo"),
        (b"BUYINGSTOREMIRRORSELLLISTWNDINFO",
         "buyingStoreMirrorSellListWndInfo"),
        (b"REPLAYRECCONTROLNWNDINFO", "replayRecControlnWndInfo"),
        (b"NAVIGATIONWNDINFO", "navigationWndInfo"),
        (b"MINIMAPWNDINFO", "minimapWndInfo"),
        (b"ACHTRACINGWNDINFO", "achTracingWndInfo"),
        (b"PREVIEWEQUIPWNDINFO", "previewEquipWndInfo"),
        (b"SWAPEQUIPMENTWNDINFO", "swapEquipmentWndInfo"),
        (b"ITEMSTOREWNDINFO", "itemStoreWndInfo"),
        (b"PETINFOWNDINFO", "petInfoWndInfo"),
        (b"HOMUNINFOWNDINFO", "homunInfoWndInfo"),
        (b"MERINFOWNDINFO", "merInfoWndInfo"),
    )
    vals2 = [
        (b"ITEMWNDINFO.ORGHEIGHT", "itemWndInfo.orgHeight"),
        (b"ITEMWNDINFO.CURTAB", "itemWndInfo.curTab"),
        (b"ITEMWNDINFO.CURRADIO1", "itemWndInfo.curRadio1"),
        (b"ITEMWNDINFO.CURRADIO2", "itemWndInfo.curRadio2"),
        (b"ITEMWNDINFO.CURRADIO3", "itemWndInfo.curRadio3"),
        (b"ITEMWNDINFO.CURRADIO4", "itemWndInfo.curRadio4"),
        (b"ITEMWNDINFO.SHOW", "itemWndInfo.show"),
        (b"QUESTWNDINFO.ORGHEIGHT", "questWndInfo.orgHeight"),
        (b"QUESTWNDINFO.CURTAB", "questWndInfo.curTab"),
        (b"QUESTWNDINFO.SHOW", "questWndInfo.SHOW"),
        (b"QUESTDISPWNDINFO.QUESTDISPLAY", "questDisplayWndInfo.questDisplay"),
        (b"STATUSWNDINFO.ORGHEIGHT", "statusWndInfo.orgHeight"),
        (b"STATUSWNDINFO.SHOW", "statusWndInfo.show"),
        (b"EQUIPWNDINFO.ORGHEIGHT", "equipWndInfo.orgHeight"),
        (b"EQUIPWNDINFO.SHOW", "equipWndInfo.show"),
        (b"OPTIONWNDINFO.ORGHEIGHT", "optionWndInfo.orgHeight"),
        (b"OPTIONWNDINFO.SHOW", "optionWndInfo.show"),
        (b"SHORTENITEMWNDINFO.SHOW", "shortenItemWndInfo.show"),
        (b"SHORTCUTWNDINFO.SHOW", "shortCutWndInfo.show"),
        (b"ITEMSTOREWNDINFO.subWnd", "itemStoreWndInfo.subWnd"),
        (b"ITEMSTOREWNDINFO.findWnd", "itemStoreWndInfo.findWnd"),
        (b"MESSENGERGROUPWNDINFO.SHOW", "messengerGroupWndInfo.show"),
        (b"MESSENGERGROUPWNDINFO.RADIO", "messengerGroupWndInfo.radio"),
        (b"MESSENGERGROUPWNDINFO.SIZEMODE", "messengerGroupWndInfo.sizeMode"),
        (b"SKILLLISTWNDINFO.SHOW", "skillListWndInfo.show"),
        (b"MERCHANTITEMWNDINFO.SHOW", "merchantItemWndInfo.show"),
        (b"NAVIGATIONWNDINFO.ISSCREEN", "navigationWndInfo.isScreen"),
        (b"MINIMAPWNDINFO.SHOWQUEST", "minimapWndInfo.showQuest"),
        (b"MINIMAPWNDINFO.WHOWNPC", "minimapWndInfo.whoWNpc"),
        (b"MINIMAPWNDINFO.WHOWMEMBER", "minimapWndInfo.whoWMember"),
        (b"BASICINFOWNDINFO.ISSHOW", "basicInfoWndInfo.isShow"),
    ]
    for val in vals:
        for prop in props:
            vals2.append((val[0] + b"." + prop[0],
                         "{0}.{1}".format(val[1], prop[1])))
    vals = vals2
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           False,
                           2)
    # structName = (offset=(fieldName, flag))
    structs1 = dict()
    # structName = set(offsets)
    structs2 = dict()
    # memberName = structName
    structNames = dict()
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            val1 = val[1]
            idx = val1.find(".")
            valName1 = val1[:idx]
            valName2 = val1[idx + 1:]
            fields = structs1.get(valName1, dict())
            offset = arr[val0]
            if type(offset) is list:
                cnt = 1
                # if more than one var found, add cnt to field name
                for data in offset:
                    valSubName = "{0}_{1}".format(valName1, cnt)
                    offsets = structs2.get(valSubName, set())
                    if cnt == 1:
                        fields[data] = valName2
                    offsets.add(data)
                    structs2[valSubName] = offsets
                    structNames[valSubName] = valName1
                    cnt = cnt + 1
            else:
                fields[offset] = valName2
                offsets = structs2.get(valName1, set())
                offsets.add(offset)
                structs2[valName1] = offsets
                structNames[valName1] = valName1
            structs1[valName1] = fields
    for struct in sorted(structs1.keys()):
        fields = structs1[struct]
        firstOffset = min(fields.keys())
        structName = struct[0].upper() + struct[1:]
        self.addStruct(structName)
        for offset in sorted(fields.keys()):
            field = fields[offset]
            self.addStructMember(field,
                                 offset - firstOffset,
                                 4,
                                 False)
    self.addStruct("UIWindowMgr")
    for name in sorted(structs2.keys()):
        offset = min(structs2[name])
        name2 = structNames[name]
        structName = name2[0].upper() + name2[1:]
        self.addStructMember(name, offset, 4, True)
        self.setStructMemberType(offset, structName)

    # UIWindowsMgr fields with absolute address
    vals = (
        (b"m_bCanSetChatMode", "m_bCanSetChatMode", 4),
    )
    from bpe.blocks.windowmgrparamsabs import blocks
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           True,
                           4)
    self.addStruct("UIWindowMgr")
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1],
                                 arr[val0] - self.gWindowMgr,
                                 val[2],
                                 True)
