#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


# search in recv_packet_9A

def searchNew(self, blueStrHex, toolStrHex):
    # 0  lea eax, [ebp+chars]
    # 6  push 1
    # 8  push 0
    # 10 push eax
    # 11 lea ecx, [ebp+message]
    # 17 mov [ebp+chars], 23h
    # 24 call std_string_find
    # 29 push 1
    # 31 mov [ebp+name_separator], eax
    # 37 push 0
    # 39 lea eax, [ebp+chars]
    # 45 push eax
    # 46 lea ecx, [ebp+message]
    # 52 mov [ebp+chars], 3Ah
    # 59 call std_string_find
    # 64 push 4
    # 66 push 0
    # 68 push offset substr
    # 73 lea ecx, [ebp+message]
    # 79 mov [ebp+message_separator], eax
    # 85 call std_string_find
    # 90 mov [ebp+chars], 1
    # 97 mov edx, offset aTool
    code = (
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 0
        b"\x6A\x01"                        # 6
        b"\x6A\x00"                        # 8
        b"\x50"                            # 10
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 11
        b"\xC6\x85\xAB\xAB\xAB\xAB\x23"    # 17
        b"\xE8\xAB\xAB\xAB\xAB"            # 24
        b"\x6A\x01"                        # 29
        b"\x89\x85\xAB\xAB\xAB\xAB"        # 31
        b"\x6A\x00"                        # 37
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 39
        b"\x50"                            # 45
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 46
        b"\xC6\x85\xAB\xAB\xAB\xAB\x3A"    # 52
        b"\xE8\xAB\xAB\xAB\xAB"            # 59
        b"\x6A\x04"                        # 64
        b"\x6A\x00"                        # 66
        b"\x68" + blueStrHex +             # 68
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 73
        b"\x89\x85\xAB\xAB\xAB\xAB"        # 79
        b"\xE8\xAB\xAB\xAB\xAB"            # 85
        b"\xC6\x85\xAB\xAB\xAB\xAB\x01"    # 90
        b"\xBA" + toolStrHex               # 97
    )
    std_string_findOffsets = (25, 60, 86)
    std_string_findRelative = True
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-01-07
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [ebp+var_C54]
        # 10 push edx
        # 11 lea ecx, [ebp+var_C50]
        # 17 mov [ebp+var_C74], 15h
        # 27 mov [ebp+var_C54], 23h
        # 34 call std_string_find
        # 39 push 1
        # 41 mov esi, eax
        # 43 push 0
        # 45 lea eax, [ebp+var_C54]
        # 51 push eax
        # 52 lea ecx, [ebp+var_C50]
        # 58 mov [ebp+var_C54], 3Ah
        # 65 call std_string_find
        # 70 push 4
        # 72 push 0
        # 74 push offset aBlue
        # 79 lea ecx, [ebp+var_C50]
        # 85 mov [ebp+var_C78], eax
        # 91 call std_string_find
        # 96 mov [ebp+var_C54], 1
        # 103 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 4
            b"\x52"                            # 10
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 11
            b"\xC7\x85\xAB\xAB\xAB\xAB\x15\x00\x00\x00"  # 17
            b"\xC6\x85\xAB\xAB\xAB\xAB\x23"    # 27
            b"\xE8\xAB\xAB\xAB\xAB"            # 34
            b"\x6A\x01"                        # 39
            b"\x8B\xF0"                        # 41
            b"\x6A\x00"                        # 43
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 45
            b"\x50"                            # 51
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 52
            b"\xC6\x85\xAB\xAB\xAB\xAB\x3A"    # 58
            b"\xE8\xAB\xAB\xAB\xAB"            # 65
            b"\x6A\x04"                        # 70
            b"\x6A\x00"                        # 72
            b"\x68" + blueStrHex +             # 74
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 79
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 85
            b"\xE8\xAB\xAB\xAB\xAB"            # 91
            b"\xC6\x85\xAB\xAB\xAB\xAB\x01"    # 96
            b"\xB9" + toolStrHex               # 103
        )
        std_string_findOffsets = (35, 66, 92)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-03-03
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [esp+0C90h+var_C74]
        # 8  push edx
        # 9  lea ecx, [esp+0C94h+var_C50]
        # 13 mov ebp, 14h
        # 18 mov byte ptr [esp+0C94h+var_C74], 23h
        # 23 call std_string_find
        # 29 push 1
        # 31 mov esi, eax
        # 33 push 0
        # 35 lea eax, [esp+0C90h+var_C74]
        # 39 push eax
        # 40 lea ecx, [esp+0C94h+var_C50]
        # 44 mov byte ptr [esp+0C94h+var_C74], 3Ah
        # 49 call std_string_find
        # 55 push 4
        # 57 push 0
        # 59 push offset aBlue
        # 64 lea ecx, [esp+0C94h+var_C50]
        # 68 mov [esp+0C94h+var_C54], eax
        # 72 call std_string_find
        # 78 mov byte ptr [esp+0C88h+var_C74], 1
        # 83 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x54\x24\xAB"                # 4
            b"\x52"                            # 8
            b"\x8D\x4C\x24\xAB"                # 9
            b"\xBD\xAB\xAB\x00\x00"            # 13
            b"\xC6\x44\x24\xAB\x23"            # 18
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 23
            b"\x6A\x01"                        # 29
            b"\x8B\xF0"                        # 31
            b"\x6A\x00"                        # 33
            b"\x8D\x44\x24\xAB"                # 35
            b"\x50"                            # 39
            b"\x8D\x4C\x24\xAB"                # 40
            b"\xC6\x44\x24\xAB\x3A"            # 44
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 49
            b"\x6A\x04"                        # 55
            b"\x6A\x00"                        # 57
            b"\x68" + blueStrHex +             # 59
            b"\x8D\x4C\x24\xAB"                # 64
            b"\x89\x44\x24\xAB"                # 68
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 72
            b"\xC6\x44\x24\xAB\x01"            # 78
            b"\xB9" + toolStrHex               # 83
        )
        std_string_findOffsets = (25, 51, 74)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2013-01-15
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [ebp+var_C54]
        # 10 push edx
        # 11 lea ecx, [ebp+var_C50]
        # 17 mov [ebp+var_C74], 14h
        # 27 mov [ebp+var_C54], 23h
        # 34 call std_string_find
        # 39 push 1
        # 41 mov esi, eax
        # 43 push 0
        # 45 lea eax, [ebp+var_C54]
        # 51 push eax
        # 52 lea ecx, [ebp+var_C50]
        # 58 mov [ebp+var_C54], 3Ah
        # 65 call std_string_find
        # 70 push 4
        # 72 push 0
        # 74 push offset aBlue
        # 79 lea ecx, [ebp+var_C50]
        # 85 mov [ebp+var_C78], eax
        # 91 call std_string_find
        # 96 mov [ebp+var_C54], 1
        # 103 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 4
            b"\x52"                            # 10
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 11
            b"\xC7\x85\xAB\xAB\xAB\xAB\x14\x00\x00\x00"  # 17
            b"\xC6\x85\xAB\xAB\xAB\xAB\x23"    # 27
            b"\xE8\xAB\xAB\xAB\xAB"            # 34
            b"\x6A\x01"                        # 39
            b"\x8B\xF0"                        # 41
            b"\x6A\x00"                        # 43
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 45
            b"\x50"                            # 51
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 52
            b"\xC6\x85\xAB\xAB\xAB\xAB\x3A"    # 58
            b"\xE8\xAB\xAB\xAB\xAB"            # 65
            b"\x6A\x04"                        # 70
            b"\x6A\x00"                        # 72
            b"\x68" + blueStrHex +             # 74
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 79
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 85
            b"\xE8\xAB\xAB\xAB\xAB"            # 91
            b"\xC6\x85\xAB\xAB\xAB\xAB\x01"    # 96
            b"\xB9" + toolStrHex               # 103
        )
        std_string_findOffsets = (35, 66, 92)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2012-01-03
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [esp+0C90h+var_C74]
        # 8  push edx
        # 9  lea ecx, [esp+0C94h+var_C50]
        # 13 mov ebp, 14h
        # 18 mov [esp+0C94h+var_C74], 23h
        # 23 call std_string_find
        # 29 push 1
        # 31 mov esi, eax
        # 33 push 0
        # 35 lea eax, [esp+0C90h+var_C74]
        # 39 push eax
        # 40 lea ecx, [esp+0C94h+var_C50]
        # 44 mov [esp+0C94h+var_C74], 3Ah
        # 49 call std_string_find
        # 55 push 4
        # 57 push 0
        # 59 push offset aBlue
        # 64 lea ecx, [esp+0C94h+var_C50]
        # 68 mov [esp+0C94h+var_C54], eax
        # 72 call std_string_find
        # 78 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x54\x24\xAB"                # 4
            b"\x52"                            # 8
            b"\x8D\x4C\x24\xAB"                # 9
            b"\xBD\xAB\xAB\x00\x00"            # 13
            b"\xC6\x44\x24\xAB\x23"            # 18
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 23
            b"\x6A\x01"                        # 29
            b"\x8B\xF0"                        # 31
            b"\x6A\x00"                        # 33
            b"\x8D\x44\x24\xAB"                # 35
            b"\x50"                            # 39
            b"\x8D\x4C\x24\xAB"                # 40
            b"\xC6\x44\x24\xAB\x3A"            # 44
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 49
            b"\x6A\x04"                        # 55
            b"\x6A\x00"                        # 57
            b"\x68" + blueStrHex +             # 59
            b"\x8D\x4C\x24\xAB"                # 64
            b"\x89\x44\x24\xAB"                # 68
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 72
            b"\xB9" + toolStrHex               # 78
        )
        std_string_findOffsets = (25, 51, 74)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2011-01-04
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C84h+var_C68]
        # 7  push edx
        # 8  lea ecx, [esp+0C88h+var_C50]
        # 12 mov ebp, 14h
        # 17 mov byte ptr [esp+0C88h+var_C68], 23h
        # 22 call std_string_find
        # 28 push 1
        # 30 mov esi, eax
        # 32 push ebx
        # 33 lea eax, [esp+0C84h+var_C68]
        # 37 push eax
        # 38 lea ecx, [esp+0C88h+var_C50]
        # 42 mov byte ptr [esp+0C88h+var_C68], 3Ah
        # 47 call std_string_find
        # 53 push 4
        # 55 push ebx
        # 56 push offset aBlue
        # 61 lea ecx, [esp+0C88h+var_C50]
        # 65 mov [esp+0C88h+var_C54], eax
        # 69 call std_string_find
        # 75 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xBD\xAB\xAB\x00\x00"            # 12
            b"\xC6\x44\x24\xAB\x23"            # 17
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 22
            b"\x6A\x01"                        # 28
            b"\x8B\xF0"                        # 30
            b"\x53"                            # 32
            b"\x8D\x44\x24\xAB"                # 33
            b"\x50"                            # 37
            b"\x8D\x4C\x24\xAB"                # 38
            b"\xC6\x44\x24\xAB\x3A"            # 42
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 47
            b"\x6A\x04"                        # 53
            b"\x53"                            # 55
            b"\x68" + blueStrHex +             # 56
            b"\x8D\x4C\x24\xAB"                # 61
            b"\x89\x44\x24\xAB"                # 65
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 69
            b"\xB9" + toolStrHex               # 75
        )
        std_string_findOffsets = (24, 49, 71)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2010-01-05
        # 0  lea ecx, [ebp+arg_0+3]
        # 3  mov byte ptr [ebp+arg_0+3], 23h
        # 7  push 1
        # 9  push 0
        # 11 push ecx
        # 12 lea ecx, [ebp+var_20]
        # 15 call std_string_find
        # 20 push 1
        # 22 lea edx, [ebp+arg_0+3]
        # 25 push 0
        # 27 push edx
        # 28 lea ecx, [ebp+var_20]
        # 31 mov ebx, eax
        # 33 mov byte ptr [ebp+arg_0+3], 3Ah
        # 37 call std_string_find
        # 42 mov [ebp+arg_0], eax
        # 45 mov edi, offset aBlue
        # 50 or ecx, 0FFFFFFFFh
        # 53 xor eax, eax
        # 55 repne scasb
        # 57 not ecx
        # 59 dec ecx
        # 60 push ecx
        # 61 push eax
        # 62 push offset aBlue
        # 67 lea ecx, [ebp+var_20]
        # 70 call std_string_find
        # 75 mov esi, offset aTool
        code = (
            b"\x8D\x4D\xAB"                    # 0
            b"\xC6\x45\xAB\x23"                # 3
            b"\x6A\x01"                        # 7
            b"\x6A\x00"                        # 9
            b"\x51"                            # 11
            b"\x8D\x4D\xAB"                    # 12
            b"\xE8\xAB\xAB\xAB\xAB"            # 15
            b"\x6A\x01"                        # 20
            b"\x8D\x55\xAB"                    # 22
            b"\x6A\x00"                        # 25
            b"\x52"                            # 27
            b"\x8D\x4D\xAB"                    # 28
            b"\x8B\xD8"                        # 31
            b"\xC6\x45\xAB\x3A"                # 33
            b"\xE8\xAB\xAB\xAB\xAB"            # 37
            b"\x89\x45\xAB"                    # 42
            b"\xBF" + blueStrHex +             # 45
            b"\x83\xC9\xFF"                    # 50
            b"\x33\xC0"                        # 53
            b"\xF2\xAE"                        # 55
            b"\xF7\xD1"                        # 57
            b"\x49"                            # 59
            b"\x51"                            # 60
            b"\x50"                            # 61
            b"\x68" + blueStrHex +             # 62
            b"\x8D\x4D\xAB"                    # 67
            b"\xE8\xAB\xAB\xAB\xAB"            # 70
            b"\xBE" + toolStrHex               # 75
        )
        std_string_findOffsets = (16, 38, 71)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2010-06-15
        # 0  lea ecx, [ebp+arg_0+3]
        # 3  mov [ebp+var_2C], 14h
        # 10 mov byte ptr [ebp+arg_0+3], 23h
        # 14 push 1
        # 16 push 0
        # 18 push ecx
        # 19 lea ecx, [ebp+var_20]
        # 22 call std_string_find
        # 27 push 1
        # 29 lea edx, [ebp+arg_0+3]
        # 32 push 0
        # 34 push edx
        # 35 lea ecx, [ebp+var_20]
        # 38 mov ebx, eax
        # 40 mov byte ptr [ebp+arg_0+3], 3Ah
        # 44 call std_string_find
        # 49 mov [ebp+arg_0], eax
        # 52 mov edi, offset aBlue
        # 57 or ecx, 0FFFFFFFFh
        # 60 xor eax, eax
        # 62 repne scasb
        # 64 not ecx
        # 66 dec ecx
        # 67 push ecx
        # 68 push eax
        # 69 push offset aBlue
        # 74 lea ecx, [ebp+var_20]
        # 77 call std_string_find
        # 82 mov esi, offset aTool
        code = (
            b"\x8D\x4D\xAB"                    # 0
            b"\xC7\x45\xAB\x14\x00\x00\x00"    # 3
            b"\xC6\x45\xAB\x23"                # 10
            b"\x6A\x01"                        # 14
            b"\x6A\x00"                        # 16
            b"\x51"                            # 18
            b"\x8D\x4D\xAB"                    # 19
            b"\xE8\xAB\xAB\xAB\xAB"            # 22
            b"\x6A\x01"                        # 27
            b"\x8D\x55\xAB"                    # 29
            b"\x6A\x00"                        # 32
            b"\x52"                            # 34
            b"\x8D\x4D\xAB"                    # 35
            b"\x8B\xD8"                        # 38
            b"\xC6\x45\xAB\x3A"                # 40
            b"\xE8\xAB\xAB\xAB\xAB"            # 44
            b"\x89\x45\xAB"                    # 49
            b"\xBF" + blueStrHex +             # 52
            b"\x83\xC9\xFF"                    # 57
            b"\x33\xC0"                        # 60
            b"\xF2\xAE"                        # 62
            b"\xF7\xD1"                        # 64
            b"\x49"                            # 66
            b"\x51"                            # 67
            b"\x50"                            # 68
            b"\x68" + blueStrHex +             # 69
            b"\x8D\x4D\xAB"                    # 74
            b"\xE8\xAB\xAB\xAB\xAB"            # 77
            b"\xBE" + toolStrHex               # 82
        )
        std_string_findOffsets = (23, 45, 78)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2009-01-13
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C84h+var_C68]
        # 7  push edx
        # 8  lea ecx, [esp+0C88h+var_C50]
        # 12 mov byte ptr [esp+0C88h+var_C68], 23h
        # 17 call std_string_find
        # 23 push 1
        # 25 mov edi, eax
        # 27 push ebx
        # 28 lea eax, [esp+0C84h+var_C68]
        # 32 push eax
        # 33 lea ecx, [esp+0C88h+var_C50]
        # 37 mov byte ptr [esp+0C88h+var_C68], 3Ah
        # 42 call std_string_find
        # 48 push 4
        # 50 push ebx
        # 51 push offset aBlue
        # 56 lea ecx, [esp+0C88h+var_C50]
        # 60 mov esi, eax
        # 62 call std_string_find
        # 68 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xC6\x44\x24\xAB\x23"            # 12
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 17
            b"\x6A\x01"                        # 23
            b"\x8B\xF8"                        # 25
            b"\x53"                            # 27
            b"\x8D\x44\x24\xAB"                # 28
            b"\x50"                            # 32
            b"\x8D\x4C\x24\xAB"                # 33
            b"\xC6\x44\x24\xAB\x3A"            # 37
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 42
            b"\x6A\x04"                        # 48
            b"\x53"                            # 50
            b"\x68" + blueStrHex +             # 51
            b"\x8D\x4C\x24\xAB"                # 56
            b"\x8B\xF0"                        # 60
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 62
            b"\xB9" + toolStrHex               # 68
        )
        std_string_findOffsets = (19, 44, 64)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2009-07-08
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C80h+var_C64]
        # 7  push edx
        # 8  lea ecx, [esp+0C84h+var_C50]
        # 12 mov byte ptr [esp+0C84h+var_C64], 23h
        # 17 call std_string_find
        # 23 push 1
        # 25 mov esi, eax
        # 27 push ebx
        # 28 lea eax, [esp+0C80h+var_C64]
        # 32 push eax
        # 33 lea ecx, [esp+0C84h+var_C50]
        # 37 mov byte ptr [esp+0C84h+var_C64], 3Ah
        # 42 call std_string_find
        # 48 push 4
        # 50 push ebx
        # 51 push offset aBlue
        # 56 lea ecx, [esp+0C84h+var_C50]
        # 60 mov [esp+0C84h+var_C54], eax
        # 64 call std_string_find
        # 70 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xC6\x44\x24\xAB\x23"            # 12
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 17
            b"\x6A\x01"                        # 23
            b"\x8B\xF0"                        # 25
            b"\x53"                            # 27
            b"\x8D\x44\x24\xAB"                # 28
            b"\x50"                            # 32
            b"\x8D\x4C\x24\xAB"                # 33
            b"\xC6\x44\x24\xAB\x3A"            # 37
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 42
            b"\x6A\x04"                        # 48
            b"\x53"                            # 50
            b"\x68" + blueStrHex +             # 51
            b"\x8D\x4C\x24\xAB"                # 56
            b"\x89\x44\x24\xAB"                # 60
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 64
            b"\xB9" + toolStrHex               # 70
        )
        std_string_findOffsets = (19, 44, 66)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2008-01-02
        # 0  lea edx, [ebp+arg_0+3]
        # 3  lea ecx, [ebp+var_20]
        # 6  mov byte ptr [ebp+arg_0+3], 23h
        # 10 push 1
        # 12 push 0
        # 14 push edx
        # 15 call std_string_find
        # 20 mov ebx, eax
        # 22 push 1
        # 24 lea eax, [ebp+arg_0+3]
        # 27 push 0
        # 29 push eax
        # 30 lea ecx, [ebp+var_20]
        # 33 mov byte ptr [ebp+arg_0+3], 3Ah
        # 37 call std_string_find
        # 42 mov [ebp+arg_0], eax
        # 45 mov edi, offset aBlue
        # 50 or ecx, 0FFFFFFFFh
        # 53 xor eax, eax
        # 55 repne scasb
        # 57 not ecx
        # 59 dec ecx
        # 60 push ecx
        # 61 push eax
        # 62 push offset aBlue
        # 67 lea ecx, [ebp+var_20]
        # 70 call std_string_find
        # 75 mov esi, offset aTool
        code = (
            b"\x8D\x55\xAB"                    # 0
            b"\x8D\x4D\xAB"                    # 3
            b"\xC6\x45\xAB\x23"                # 6
            b"\x6A\x01"                        # 10
            b"\x6A\x00"                        # 12
            b"\x52"                            # 14
            b"\xE8\xAB\xAB\xAB\xAB"            # 15
            b"\x8B\xD8"                        # 20
            b"\x6A\x01"                        # 22
            b"\x8D\x45\xAB"                    # 24
            b"\x6A\x00"                        # 27
            b"\x50"                            # 29
            b"\x8D\x4D\xAB"                    # 30
            b"\xC6\x45\xAB\x3A"                # 33
            b"\xE8\xAB\xAB\xAB\xAB"            # 37
            b"\x89\x45\xAB"                    # 42
            b"\xBF" + blueStrHex +             # 45
            b"\x83\xC9\xFF"                    # 50
            b"\x33\xC0"                        # 53
            b"\xF2\xAE"                        # 55
            b"\xF7\xD1"                        # 57
            b"\x49"                            # 59
            b"\x51"                            # 60
            b"\x50"                            # 61
            b"\x68" + blueStrHex +             # 62
            b"\x8D\x4D\xAB"                    # 67
            b"\xE8\xAB\xAB\xAB\xAB"            # 70
            b"\xBE" + toolStrHex               # 75
        )
        std_string_findOffsets = (16, 38, 71)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2008-05-20
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C80h+var_C64]
        # 7  push edx
        # 8  lea ecx, [esp+0C84h+var_C50]
        # 12 mov byte ptr [esp+0C84h+var_C64], 23h
        # 17 call std_string_find
        # 22 push 1
        # 24 mov edi, eax
        # 26 push ebx
        # 27 lea eax, [esp+0C80h+var_C64]
        # 31 push eax
        # 32 lea ecx, [esp+0C84h+var_C50]
        # 36 mov byte ptr [esp+0C84h+var_C64], 3Ah
        # 41 call std_string_find
        # 46 push 4
        # 48 push ebx
        # 49 push offset aBlue
        # 54 lea ecx, [esp+0C84h+var_C50]
        # 58 mov esi, eax
        # 60 call std_string_find
        # 65 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xC6\x44\x24\xAB\x23"            # 12
            b"\xE8\xAB\xAB\xAB\xAB"            # 17
            b"\x6A\x01"                        # 22
            b"\x8B\xF8"                        # 24
            b"\x53"                            # 26
            b"\x8D\x44\x24\xAB"                # 27
            b"\x50"                            # 31
            b"\x8D\x4C\x24\xAB"                # 32
            b"\xC6\x44\x24\xAB\x3A"            # 36
            b"\xE8\xAB\xAB\xAB\xAB"            # 41
            b"\x6A\x04"                        # 46
            b"\x53"                            # 48
            b"\x68" + blueStrHex +             # 49
            b"\x8D\x4C\x24\xAB"                # 54
            b"\x8B\xF0"                        # 58
            b"\xE8\xAB\xAB\xAB\xAB"            # 60
            b"\xB9" + toolStrHex               # 65
        )
        std_string_findOffsets = (18, 42, 61)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2008-11-11
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C80h+var_C64]
        # 7  push edx
        # 8  lea ecx, [esp+0C84h+var_C50]
        # 12 mov byte ptr [esp+0C84h+var_C64], 23h
        # 17 call std_string_find
        # 23 push 1
        # 25 mov esi, eax
        # 27 push ebx
        # 28 lea eax, [esp+0C80h+var_C64]
        # 32 push eax
        # 33 lea ecx, [esp+0C84h+var_C50]
        # 37 mov byte ptr [esp+0C84h+var_C64], 3Ah
        # 42 call std_string_find
        # 48 push 4
        # 50 push ebx
        # 51 push offset aBlue
        # 56 lea ecx, [esp+0C84h+var_C50]
        # 60 mov edi, eax
        # 62 call std_string_find
        # 68 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xC6\x44\x24\xAB\x23"            # 12
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 17
            b"\x6A\x01"                        # 23
            b"\x8B\xF0"                        # 25
            b"\x53"                            # 27
            b"\x8D\x44\x24\xAB"                # 28
            b"\x50"                            # 32
            b"\x8D\x4C\x24\xAB"                # 33
            b"\xC6\x44\x24\xAB\x3A"            # 37
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 42
            b"\x6A\x04"                        # 48
            b"\x53"                            # 50
            b"\x68" + blueStrHex +             # 51
            b"\x8D\x4C\x24\xAB"                # 56
            b"\x8B\xF8"                        # 60
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 62
            b"\xB9" + toolStrHex               # 68
        )
        std_string_findOffsets = (19, 44, 64)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2018-08-13 ru
        # 0  push 1
        # 2  push 0
        # 4  push eax
        # 5  lea ecx, [ebp+var_C4C]
        # 11 mov [ebp+var_C54], 14h
        # 21 mov [ebp+var_C50], 23h
        # 28 call std_string_find
        # 33 push 1
        # 35 mov [ebp+Size], eax
        # 41 push 0
        # 43 lea eax, [ebp+var_C50]
        # 49 push eax
        # 50 lea ecx, [ebp+var_C4C]
        # 56 mov [ebp+var_C50], 3Ah
        # 63 call std_string_find
        # 68 push 4
        # 70 push 0
        # 72 push offset aBlue
        # 77 lea ecx, [ebp+var_C4C]
        # 83 mov [ebp+var_C74], eax
        # 89 call std_string_find
        # 94 mov [ebp+var_C50], 1
        # 101 mov edx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x50"                            # 4
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 5
            b"\xC7\x85\xAB\xAB\xAB\xFF\xAB\xAB\x00\x00"  # 11
            b"\xC6\x85\xAB\xAB\xAB\xAB\x23"    # 21
            b"\xE8\xAB\xAB\xAB\xAB"            # 28
            b"\x6A\x01"                        # 33
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 35
            b"\x6A\x00"                        # 41
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 43
            b"\x50"                            # 49
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 50
            b"\xC6\x85\xAB\xAB\xAB\xAB\x3A"    # 56
            b"\xE8\xAB\xAB\xAB\xAB"            # 63
            b"\x6A\x04"                        # 68
            b"\x6A\x00"                        # 70
            b"\x68" + blueStrHex +             # 72
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 77
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 83
            b"\xE8\xAB\xAB\xAB\xAB"            # 89
            b"\xC6\x85\xAB\xAB\xAB\xAB\x01"    # 94
            b"\xBA" + toolStrHex               # 101
        )
        std_string_findOffsets = (29, 64, 90)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-04-06 iro
        # 0  push 1
        # 2  push ebx
        # 3  lea edx, [esp+0C84h+var_C68]
        # 7  push edx
        # 8  lea ecx, [esp+0C88h+var_C50]
        # 12 mov [esp+0C88h+var_C5C], 14h
        # 20 mov byte ptr [esp+0C88h+var_C68], 23h
        # 25 call ds:std_string_find
        # 31 push 1
        # 33 mov esi, eax
        # 35 push ebx
        # 36 lea eax, [esp+0C84h+var_C68]
        # 40 push eax
        # 41 lea ecx, [esp+0C88h+var_C50]
        # 45 mov byte ptr [esp+0C88h+var_C68], 3Ah
        # 50 call ds:std_string_find
        # 56 push 4
        # 58 push ebx
        # 59 push offset aBlue
        # 64 lea ecx, [esp+0C88h+var_C50]
        # 68 mov [esp+0C88h+var_C54], eax
        # 72 call ds:std_string_find
        # 78 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x53"                            # 2
            b"\x8D\x54\x24\xAB"                # 3
            b"\x52"                            # 7
            b"\x8D\x4C\x24\xAB"                # 8
            b"\xC7\x44\x24\xAB\xAB\xAB\x00\x00"  # 12
            b"\xC6\x44\x24\xAB\x23"            # 20
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 25
            b"\x6A\x01"                        # 31
            b"\x8B\xF0"                        # 33
            b"\x53"                            # 35
            b"\x8D\x44\x24\xAB"                # 36
            b"\x50"                            # 40
            b"\x8D\x4C\x24\xAB"                # 41
            b"\xC6\x44\x24\x20\xAB"            # 45
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 50
            b"\x6A\x04"                        # 56
            b"\x53"                            # 58
            b"\x68" + blueStrHex +             # 59
            b"\x8D\x4C\x24\xAB"                # 64
            b"\x89\x44\x24\xAB"                # 68
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 72
            b"\xB9" + toolStrHex               # 78
        )
        std_string_findOffsets = (27, 52, 74)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-07-22 iro
        # 0  lea ecx, [ebp+arg_0+3]
        # 3  mov byte ptr [ebp+arg_0+3], 23h
        # 7  push 1
        # 9  push 0
        # 11 push ecx
        # 12 lea ecx, [ebp+var_20]
        # 15 call std_string_find
        # 20 push 1
        # 22 lea edx, [ebp+arg_0+3]
        # 25 push 0
        # 27 push edx
        # 28 lea ecx, [ebp+var_20]
        # 31 mov ebx, eax
        # 33 mov byte ptr [ebp+arg_0+3], 3Ah
        # 37 call std_string_find
        # 42 mov [ebp+arg_0], eax
        # 45 mov edi, offset aBlue
        # 50 or ecx, 0FFFFFFFFh
        # 53 xor eax, eax
        # 55 repne scasb
        # 57 not ecx
        # 59 dec ecx
        # 60 push ecx
        # 61 push eax
        # 62 push offset aBlue
        # 67 lea ecx, [ebp+var_20]
        # 70 call std_string_find
        # 75 mov esi, offset aTool
        code = (
            b"\x8D\x4D\xAB"                    # 0
            b"\xC6\x45\xAB\xAB"                # 3
            b"\x6A\x01"                        # 7
            b"\x6A\x00"                        # 9
            b"\x51"                            # 11
            b"\x8D\x4D\xAB"                    # 12
            b"\xE8\xAB\xAB\xAB\xAB"            # 15
            b"\x6A\x01"                        # 20
            b"\x8D\x55\xAB"                    # 22
            b"\x6A\x00"                        # 25
            b"\x52"                            # 27
            b"\x8D\x4D\xAB"                    # 28
            b"\x8B\xD8"                        # 31
            b"\xC6\x45\xAB\x3A"                # 33
            b"\xE8\xAB\xAB\xAB\xAB"            # 37
            b"\x89\x45\xAB"                    # 42
            b"\xBF" + blueStrHex +             # 45
            b"\x83\xC9\xFF"                    # 50
            b"\x33\xC0"                        # 53
            b"\xF2\xAE"                        # 55
            b"\xF7\xD1"                        # 57
            b"\x49"                            # 59
            b"\x51"                            # 60
            b"\x50"                            # 61
            b"\x68" + blueStrHex +             # 62
            b"\x8D\x4D\xAB"                    # 67
            b"\xE8\xAB\xAB\xAB\xAB"            # 70
            b"\xBE" + toolStrHex               # 75
        )
        std_string_findOffsets = (16, 38, 71)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-10-24 iro
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [esp+0C90h+var_C74]
        # 8  push edx
        # 9  lea ecx, [esp+0C94h+var_C50]
        # 13 mov [esp+0C94h+var_C68], 14h
        # 21 mov [esp+0C94h+var_C74], 23h
        # 26 call ds:std_string_find
        # 32 push 1
        # 34 mov esi, eax
        # 36 push 0
        # 38 lea eax, [esp+0C90h+var_C74]
        # 42 push eax
        # 43 lea ecx, [esp+0C94h+var_C50]
        # 47 mov [esp+0C94h+var_C74], 3Ah
        # 52 call ds:std_string_find
        # 58 push 4
        # 60 push 0
        # 62 push offset aBlue
        # 67 lea ecx, [esp+0C94h+var_C50]
        # 71 mov ebp, eax
        # 73 call ds:std_string_find
        # 79 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x54\x24\xAB"                # 4
            b"\x52"                            # 8
            b"\x8D\x4C\x24\xAB"                # 9
            b"\xC7\x44\x24\xAB\xAB\xAB\x00\x00"  # 13
            b"\xC6\x44\x24\xAB\x23"            # 21
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 26
            b"\x6A\x01"                        # 32
            b"\x8B\xF0"                        # 34
            b"\x6A\x00"                        # 36
            b"\x8D\x44\x24\xAB"                # 38
            b"\x50"                            # 42
            b"\x8D\x4C\x24\xAB"                # 43
            b"\xC6\x44\x24\xAB\x3A"            # 47
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 52
            b"\x6A\x04"                        # 58
            b"\x6A\x00"                        # 60
            b"\x68" + blueStrHex +             # 62
            b"\x8D\x4C\x24\xAB"                # 67
            b"\x8B\xE8"                        # 71
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 73
            b"\xB9" + toolStrHex               # 79
        )
        std_string_findOffsets = (28, 54, 75)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2012-06-28 iro
        # 0  push 1
        # 2  push 0
        # 4  lea edx, [esp+0C90h+var_C74]
        # 8  push edx
        # 9  lea ecx, [esp+0C94h+var_C50]
        # 13 mov ebp, 13h
        # 18 mov [esp+0C94h+var_C74], 23h
        # 23 call ds:std_string_find
        # 29 push 1
        # 31 mov esi, eax
        # 33 push 0
        # 35 lea eax, [esp+0C90h+var_C74]
        # 39 push eax
        # 40 lea ecx, [esp+0C94h+var_C50]
        # 44 mov [esp+0C94h+var_C74], 3Ah
        # 49 call ds:std_string_find
        # 55 push 4
        # 57 push 0
        # 59 push offset aBlue
        # 64 lea ecx, [esp+0C94h+var_C50]
        # 68 mov ebx, eax
        # 70 call ds:std_string_find
        # 76 mov ecx, offset aTool
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8D\x54\x24\xAB"                # 4
            b"\x52"                            # 8
            b"\x8D\x4C\x24\xAB"                # 9
            b"\xBD\xAB\xAB\x00\x00"            # 13
            b"\xC6\x44\x24\xAB\x23"            # 18
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 23
            b"\x6A\x01"                        # 29
            b"\x8B\xF0"                        # 31
            b"\x6A\x00"                        # 33
            b"\x8D\x44\x24\xAB"                # 35
            b"\x50"                            # 39
            b"\x8D\x4C\x24\xAB"                # 40
            b"\xC6\x44\x24\xAB\x3A"            # 44
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 49
            b"\x6A\x04"                        # 55
            b"\x6A\x00"                        # 57
            b"\x68" + blueStrHex +             # 59
            b"\x8D\x4C\x24\xAB"                # 64
            b"\x8B\xD8"                        # 68
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 70
            b"\xB9" + toolStrHex               # 76
        )
        std_string_findOffsets = (25, 51, 72)
        std_string_findRelative = False
        offset = self.exe.codeWildcard(code, b"\xAB")

    return (offset, std_string_findOffsets, std_string_findRelative)


def searchStdStringFind(self):
    offset, section = self.exe.string(b"blue")
    if offset is False:
        self.log("failed in search 'blue'")
        exit(1)
    blueStr = section.rawToVa(offset)

    offset, section = self.exe.string(b"tool")
    if offset is False:
        toolStrHex = ""
    else:
        toolStr = section.rawToVa(offset)
        toolStrHex = self.exe.toHex(toolStr, 4)

    blueStrHex = self.exe.toHex(blueStr, 4)

    if toolStrHex != "":
        offset, std_string_findOffsets, std_string_findRelative = \
            searchNew(self, blueStrHex, toolStrHex)

    if offset is False:
        # 2007-01-02
        # 0  push 1
        # 2  push 0
        # 4  push edx
        # 5  call std_string_find
        # 10 mov ebx, eax
        # 12 push 1
        # 14 lea eax, [ebp+arg_0+3]
        # 17 push 0
        # 19 push eax
        # 20 lea ecx, [ebp+var_1C]
        # 23 mov byte ptr [ebp+arg_0+3], 3Ah
        # 27 call std_string_find
        # 32 mov [ebp+arg_0], eax
        # 35 mov edi, offset aBlue
        # 40 or ecx, 0FFFFFFFFh
        # 43 xor eax, eax
        # 45 repne scasb
        # 47 not ecx
        # 49 dec ecx
        # 50 push ecx
        # 51 push eax
        # 52 push offset aBlue
        # 57 lea ecx, [ebp+var_1C]
        # 60 call std_string_find
        code = (
            b"\x6A\x01"                        # 0
            b"\x6A\x00"                        # 2
            b"\x52"                            # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 5
            b"\x8B\xD8"                        # 10
            b"\x6A\x01"                        # 12
            b"\x8D\x45\xAB"                    # 14
            b"\x6A\x00"                        # 17
            b"\x50"                            # 19
            b"\x8D\x4D\xAB"                    # 20
            b"\xC6\x45\xAB\x3A"                # 23
            b"\xE8\xAB\xAB\xAB\xAB"            # 27
            b"\x89\x45\xAB"                    # 32
            b"\xBF" + blueStrHex +             # 35
            b"\x83\xC9\xFF"                    # 40
            b"\x33\xC0"                        # 43
            b"\xF2\xAE"                        # 45
            b"\xF7\xD1"                        # 47
            b"\x49"                            # 49
            b"\x51"                            # 50
            b"\x50"                            # 51
            b"\x68" + blueStrHex +             # 52
            b"\x8D\x4D\xAB"                    # 57
            b"\xE8"                            # 60
        )
        std_string_findOffsets = (6, 28, 61)
        std_string_findRelative = True
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search std::string::find")
        if self.packetVersion < "20190213":
            exit(1)
        return

    self.std_string_find = 0
    for strOffset in std_string_findOffsets:
        if std_string_findRelative is True:
            std_string_find = self.getAddr(offset,
                                           strOffset,
                                           strOffset + 4)
            std_string_find = self.exe.rawToVa(std_string_find)
        else:
            std_string_find = self.exe.readUInt(offset + strOffset)
        if self.std_string_find != 0:
            if std_string_find != self.std_string_find:
                self.log("Error: found different std::string::find")
                exit(1)
        else:
            self.std_string_find = std_string_find

    self.addVaFuncType("std::string::find",
                       self.std_string_find,
                       "int __thiscall std_string_find("
                       "struct_std_string *str,"
                       " char *substr, unsigned __int32 pos, __int32 len)")
