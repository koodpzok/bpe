#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.switch import searchSwitches, labelSwitches
from bpe.blocks.switch import blocks
from bpe.blocks.switchchain import chains


def searchMapPackets(self):
    data = searchSwitches(self,
                          blocks,
                          chains,
                          self.gamePollAddr,
                          self.gamePollAddr + 0x500)
    if data is False:
        self.log("Error: map switch not found")
        exit(1)
    print("found {0} map switch values".format(len(data[1])))
    labelSwitches(self, data, "switch_packet_map{0}")
#    printSwitchAddr(self, data)
