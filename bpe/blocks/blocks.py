#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def showError(self, ret, block):
    if ret is not False:
        self.log("Error: found duplicate matched block")
        self.log("{0}:\n {1}".format(ret[0], ret[0][0].encode("hex")))
        self.log("{0}:\n {1}".format(block, block[0].encode("hex")))
        exit(1)


def matchRelativeBlock(self, blocks, offset, offset2):
    ret = []
    for block in blocks:
        code = block[0]
        retOffset = self.exe.codeWildcard(code,
                                          b"\xAB",
                                          offset,
                                          offset2)
        if retOffset is not False:
            ret.append((retOffset, block))
    return ret


def matchBlock(self, blocks, offset, relativeVars, mainVar):
    ret = False
    rawVaDiff = self.exe.codeSection.rawVaDiff
    matchWildcardOffset = self.exe.matchWildcardOffset
    getAddr = self.getAddr
    readUInt = self.exe.readUInt
    readUByte = self.exe.readUByte
    readUWord = self.exe.readUWord
    for block in blocks:
        blockOffsets = block[1]
        offset1 = offset - blockOffsets["fixedOffset"]
        if matchWildcardOffset(block[0], b"\xAB", offset1) is True:
            found = True
            blockRelOffsets = block[2]
            for var in relativeVars:
                # skip variable check what not present in block
                if var not in blockRelOffsets:
                    continue
                relOffset = blockRelOffsets[var]
                relative = True
                if type(relOffset) is tuple:
                    relative = relOffset[1]
                    relOffset = relOffset[0]
                relVal = relativeVars[var]
                if relative is True:
                    if getAddr(offset1,
                               relOffset,
                               relOffset + 4) != relVal:
                        found = False
                        break
                else:
                    if readUInt(offset1 + relOffset) != relVal:
                        found = False
                        break
            if found is False:
                continue
            retOffset = blockOffsets["retOffset"]
            if retOffset == -1:
                showError(self, ret, block)
                ret = (block, -1, 0, -1)
                continue
            if mainVar == "":
                showError(self, ret, block)
                ret = (block, offset1 + retOffset, 0, offset)
                continue
            mainOffset = blockOffsets[mainVar]
            mainVal = 0
            if type(mainOffset) is tuple:
                sz = mainOffset[1]
                mainOffset = mainOffset[0]
                if sz is False:
                    mainVal = getAddr(offset1,
                                      mainOffset,
                                      mainOffset + 4) - rawVaDiff
                elif sz == 1:
                    mainVal = readUByte(offset1 + mainOffset)
                elif sz == 2:
                    mainVal = readUWord(offset1 + mainOffset)
                elif sz == 4:
                    mainVal = readUInt(offset1 + mainOffset)
                else:
                    self.log("Error: unknown main var type size: {0}".
                             format(sz))
                    exit(1)
            else:
                mainVal = readUInt(offset1 + mainOffset)
            showError(self, ret, block)
            ret = (block, offset1 + retOffset, mainVal, offset)
#            print "{0}, {1}".format(block[0].encode("hex"),
#                                    self.exe.rawToVa(offset))
            continue
    return ret


def searchBlocks(self,
                 name,
                 offsets,
                 blocks,
                 relativeVars,
                 mainVar,
                 showStat=True):
    found = []
    errors = []
    if offsets is not False:
        for offset in offsets:
            # print hex(self.exe.rawToVa(offset))
            val = matchBlock(self, blocks, offset, relativeVars, mainVar)
            if val is False:
                errors.append(offset)
            else:
                if val[1] == -1:
                    pass  # ignored block
                else:
                    found.append(val)
    if showStat is True:
        if (len(errors) > 0 or len(found) > 0):
            errMsg = ""
            foundMsg = ""
            if len(errors) > 0:
                errMsg = " errors {0}".format(len(errors))
            if len(found) > 0:
                foundMsg = " found {0}".format(len(found))
            self.log("{0}:{1}{2}.".format(name, foundMsg, errMsg))
        else:
            self.log("{0}: found nothing.".format(name))
    return (found, errors)


def blocksToLabels(self, found, labelName):
    # id = set(addr,)
    packetToAddr = dict()
    # addr = label
    labels = dict()
    for f in found:
        addr = self.exe.rawToVa(f[1])
        packetId = hex(f[2])[2:]
        addrs = packetToAddr.get(packetId, set())
        addrs.add(addr)
        packetToAddr[packetId] = addrs
    for packetId in packetToAddr:
        addrs = sorted(packetToAddr[packetId])
        if len(addrs) == 1:
            labels[addrs[0]] = labelName.format(packetId)
        else:
            idx = 1
            for addr in addrs:
                labels[addr] = labelName.format("{0}_{1}".
                                                format(packetId, idx))
                idx = idx + 1
    for addr in sorted(labels.keys()):
        label = labels[addr]
        self.addVaLabel(label, addr, False)
    return labels


def showBlocksInfo(self, offsets, found, errors):
    self.log("total addresses: {0}".format(len(offsets)))
    self.log("found blocks: {0}".format(len(found)))
    for f in found:
        self.log(" {0}: {1}".format(hex(self.exe.rawToVa(f[1])), hex(f[2])))
    self.log("errors blocks: {0}".format(len(errors)))
    for offset in errors:
        self.log(" {0}".format(hex(self.exe.rawToVa(offset))))
