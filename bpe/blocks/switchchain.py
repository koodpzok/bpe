#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


chains = [
    # method 1
    # 2008-06-17
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFCD8h
            b"\x83\xF8\xAB"              # 5  cmp eax, 9
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_5C9386
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 14 jmp ds:off_5C9CC4[eax*4]
        ),
        {
            "add": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 17,
        },
        {
            "method": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x83\xC0\xAB"              # 0  add eax, 0FFFFFF85h
            b"\x83\xF8\xAB"              # 3  cmp eax, 0Bh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 6  ja loc_5C9386
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 12 jmp ds:off_5C93C0[eax*4]
        ),
        {
            "add": (2, 1),
            "cmp": (5, 1),
            "default": (8, False),
            "switch": 15,
        },
        {
            "method": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2008-11-19
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFD4Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 18Ch
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_5C2524
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 16 jmp ds:off_5CCCB4[eax*4]
        ),
        {
            "add": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 19,
        },
        {
            "method": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2008-11-19
    [
        (
            b"\x83\xC0\xAB"              # 0  add eax, 0FFFFFF88h
            b"\x83\xF8\xAB"              # 3  cmp eax, 3
            b"\x77\xAB"                  # 6  ja short loc_5C2524
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 8  jmp ds:off_5CC5AC[eax*4]
        ),
        {
            "add": (2, 1),
            "cmp": (5, 1),
            "default": (7, 1, False),
            "switch": 11,
        },
        {
            "method": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],




    # method 2
    # 2010-01-05
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFE79h
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0B6h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_5FDCA9
            b"\x33\xC9"                  # 16 xor ecx, ecx
            b"\x8A\x88\xAB\xAB\xAB\xAB"  # 18 mov cl, ds:byte_5FDDC8[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 24 jmp ds:off_5FDDA4[ecx*4]
        ),
        {
            "add": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 27,
            "switch1": 20,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2010-06-15
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFE79h
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0B6h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_60ABFB
            b"\x33\xD2"                  # 16 xor edx, edx
            b"\x8A\x90\xAB\xAB\xAB\xAB"  # 18 mov dl, ds:byte_60AD38[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 24 jmp ds:off_60AD14[edx*4]
        ),
        {
            "add": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 27,
            "switch1": 20,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFF7D8h
            b"\x83\xF8\xAB"              # 5  cmp eax, 16h
            b"\x77\xAB"                  # 8  ja short loc_60B468
            b"\x33\xC9"                  # 10 xor ecx, ecx
            b"\x8A\x88\xAB\xAB\xAB\xAB"  # 12 mov cl, ds:byte_60B608[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 18 jmp ds:off_60B5F4[ecx*4]
        ),
        {
            "add": 1,
            "cmp": (7, 1),
            "default": (9, 1, False),
            "switch": 21,
            "switch1": 14,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFDF3h
            b"\x83\xF8\xAB"              # 5  cmp eax, 7Eh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_60B468
            b"\x33\xC9"                  # 14 xor ecx, ecx
            b"\x8A\x88\xAB\xAB\xAB\xAB"  # 16 mov cl, ds:byte_60B574[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 22 jmp ds:off_60B560[ecx*4]
        ),
        {
            "add": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 25,
            "switch1": 18,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFE79h
            b"\x83\xF8\xAB"              # 5  cmp eax, 55h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_60B468
            b"\x33\xD2"                  # 14 xor edx, edx
            b"\x8A\x90\xAB\xAB\xAB\xAB"  # 16 mov dl, ds:byte_60B508[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 22 jmp ds:off_60B4F0[edx*4]
        ),
        {
            "add": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 25,
            "switch1": 18,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFF7D6h
            b"\x83\xF8\xAB"              # 5  cmp eax, 16h
            b"\x77\xAB"                  # 8  ja short loc_60FDF1
            b"\x33\xD2"                  # 10 xor edx, edx
            b"\x8A\x90\xAB\xAB\xAB\xAB"  # 12 mov dl, ds:byte_60FF94[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 18 jmp ds:off_60FF80[edx*4]
        ),
        {
            "add": 1,
            "cmp": (7, 1),
            "default": (9, 1, False),
            "switch": 21,
            "switch1": 14,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2009-01-07
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFDC3h
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0AEh
            b"\x77\xAB"                  # 10 ja short loc_5DAE46
            b"\x33\xC9"                  # 12 xor ecx, ecx
            b"\x8A\x88\xAB\xAB\xAB\xAB"  # 14 mov cl, ds:byte_5DAEF0[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 20 jmp ds:off_5DAED0[ecx*4]
        ),
        {
            "add": 1,
            "cmp": 6,
            "default": (11, 1, False),
            "switch": 23,
            "switch1": 16,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2008-05-07
    [
        (
            b"\x05\xAB\xAB\xAB\xAB"      # 0  add eax, 0FFFFFD7Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0B3h
            b"\x77\xAB"                  # 10 ja short loc_5C4044
            b"\x33\xD2"                  # 12 xor edx, edx
            b"\x8A\x90\xAB\xAB\xAB\xAB"  # 14 mov dl, ds:byte_5C4194[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 20 jmp ds:off_5C4174[edx*4]
        ),
        {
            "add": 1,
            "cmp": 6,
            "default": (11, 1, False),
            "switch": 23,
            "switch1": 16,
        },
        {
            "method": 2,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],



    # method 5
    # 2019-06-05
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 82Ah
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0D3h ; '?
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_A63CA8
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 16 movzx eax, ds:byte_A63ED4[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 23 jmp ds:off_A63E90[eax*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 26,
            "switch1": 19,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2018-01-03
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 0A73h
            b"\x83\xF8\xAB"              # 5  cmp eax, 70h
            b"\x77\xAB"                  # 8  ja short loc_A59E49
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 10 movzx eax, byte_A5A164[eax]
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 17 jmp off_A5A148[eax*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (9, 1, False),
            "switch": 20,
            "switch1": 13,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2018-07-04
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 0A73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 8Fh
            b"\x77\xAB"                  # 10 ja short loc_9D15CF
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 12 movzx eax, ds:byte_9D18F4[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 19 jmp ds:off_9D18D0[eax*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (11, 1, False),
            "switch": 22,
            "switch1": 15,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2014-01-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 15Fh
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0C4h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_8346F6
            b"\x0F\xB6\x90\xAB\xAB\xAB\xAB"  # 16 movzx edx, byte_834BD0[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 23 jmp off_834A1C[edx*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 26,
            "switch1": 19,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2014-01-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 853h
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 1A4h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_8346F6
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 16 movzx ecx, byte_8353C8[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 23 jmp off_835104[ecx*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 26,
            "switch1": 19,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2014-01-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 7D9h
            b"\x83\xF8\xAB"              # 5  cmp eax, 72h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_8346F6
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 14 movzx ecx, byte_835090[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 21 jmp off_834FD4[ecx*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 24,
            "switch1": 17,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2019-06-05
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 99Dh
            b"\x83\xF8\xAB"              # 5  cmp eax, 25h ; '%'
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_A63CA8
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 14 movzx eax, ds:byte_A63FE4[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 21 jmp ds:off_A63FCC[eax*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 24,
            "switch1": 17,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2014-01-29
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 436h
            b"\x83\xF8\xAB"              # 5  cmp eax, 10h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_83759A
            b"\x0F\xB6\x90\xAB\xAB\xAB\xAB"  # 14 movzx edx, byte_837D90[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 21 jmp off_837D6C[edx*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 24,
            "switch1": 17,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2011-01-04
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 82Ch
            b"\x83\xF8\xAB"              # 5  cmp eax, 25h
            b"\x77\xAB"                  # 8  ja short loc_6197A1
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 10 movzx ecx, ds:byte_61995C[eax
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 17 jmp ds:off_619948[ecx*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (9, 1, False),
            "switch": 20,
            "switch1": 13,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # aro
    [
        (
            b"\x83\xE8\xAB"              # 0  sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 3  cmp eax, 197h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_6F9800
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 14 movzx ecx, ds:byte_6F9C14[eax
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 21 jmp ds:off_6F9848[ecx*4]
        ),
        {
            "sub": (2, 1),
            "cmp": 4,
            "default": (10, False),
            "switch": 24,
            "switch1": 17,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # aro
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 82Ah
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 0BAh
            b"\x77\xAB"                  # 10 ja short loc_71015B
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 12 movzx ecx, ds:byte_71033C[eax
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 19 jmp ds:off_710324[ecx*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (11, 1, False),
            "switch": 22,
            "switch1": 15,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # bro 2018-04-12
    [
        (
            b"\x83\xE8\xAB"             # 0  sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"     # 3  cmp eax, 197h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_8D558E
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 14 movzx eax, byte_8D5974[eax]
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 21 jmp off_8D55A8[eax*4]
        ),
        {
            "sub": (2, 1),
            "cmp": 4,
            "default": (10, False),
            "switch": 24,
            "switch1": 17,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # cro 2004-03-09
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"     # 0  sub eax, 187h
            b"\x83\xF8\xAB"             # 5  cmp eax, 6Ah
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_53573C
            b"\x33\xD2"                 # 14 xor edx, edx
            b"\x8A\x90\xAB\xAB\xAB\xAB"  # 16 mov dl, ds:byte_5357C8[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 22 jmp ds:off_5357AC[edx*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 25,
            "switch1": 18,
        },
        {
            "method": 5,
            "switch1Step": 1,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],



    # method 6
    # 2014-01-08
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 43Dh
            b"\x83\xF8\xAB"              # 5  cmp eax, 5
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 8  ja loc_85C5B0
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 14 jmp switch7[eax*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (10, False),
            "switch": 17,
        },
        {
            "method": 6,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # 2013-03-27
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 99Dh
            b"\x83\xF8\xAB"              # 5  cmp eax, 8
            b"\x77\xAB"                  # 8  ja short loc_85D6FC
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 10 jmp off_85D9C0[eax*4]
        ),
        {
            "sub": 1,
            "cmp": (7, 1),
            "default": (9, 1, False),
            "switch": 13,
        },
        {
            "method": 6,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # cro 2004-03-09
    [
        (
            b"\x83\xE8\xAB"             # 0  sub eax, 69h
            b"\x83\xF8\xAB"             # 3  cmp eax, 0Bh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 6  ja loc_53573C
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 12 jmp ds:off_53577C[eax*4]
        ),
        {
            "sub": (2, 1),
            "cmp": (5, 1),
            "default": (8, False),
            "switch": 15,
        },
        {
            "method": 6,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],
    # iro 2018-05-03
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"     # 0  sub eax, 7D8h
            b"\x3D\xAB\xAB\xAB\xAB"     # 5  cmp eax, 30Dh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 10 ja loc_8AE8B1
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 16 jmp ds:off_8AF070[eax*4]
        ),
        {
            "sub": 1,
            "cmp": 6,
            "default": (12, False),
            "switch": 19,
        },
        {
            "method": 6,
            "switchStep": 4,
            "fixedOffset": 0,
            "ignore": False,
        }
    ],





    # method 7
    # 2014-01-15
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 851h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 5  jg loc_8332FE
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 11 jz loc_831670
        ),
        {
            "cmp": 1,
            "jg": (7, False),
            "jz": (13, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 17,
        }
    ],
    # 2019-01-09
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 0A4Dh
            b"\x7F\xAB"                  # 5  jg short loc_9DE674
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 7  jz loc_9DE749
        ),
        {
            "cmp": 1,
            "jg": (6, 1, False),
            "jz": (9, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 13,
        }
    ],
    # 2014-01-15
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 446h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 5  jg loc_85C36D
            b"\x74\xAB"                  # 11 jz short loc_85C35C
        ),
        {
            "cmp": 1,
            "jg": (7, False),
            "jz": (12, 1, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 13,
        }
    ],
    # 2013-01-03
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 446h
            b"\x7F\xAB"                  # 5  jg short loc_7F582E
            b"\x74\xAB"                  # 7  jz short loc_7F581D
        ),
        {
            "cmp": 1,
            "jg": (6, 1, False),
            "jz": (8, 1, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 9,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 7Ah
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 3  jg loc_5BDA16
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 9  jz loc_5C9392
        ),
        {
            "cmp": (2, 1),
            "jg": (5, False),
            "jz": (11, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 15,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 76h
            b"\x7F\xAB"                  # 3  jg short loc_5BD9DD
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5C9392
        ),
        {
            "cmp": (2, 1),
            "jg": (4, 1, False),
            "jz": (7, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 11,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 73h
            b"\x7F\xAB"                  # 3  jg short loc_5BD9CF
            b"\x74\xAB"                  # 5  jz short loc_5BD9BC
        ),
        {
            "cmp": (2, 1),
            "jg": (4, 1, False),
            "jz": (6, 1, False),
        },
        {
            "method": 7,
            "fixedOffset": 0,
            "ignore": False,
            "next": 7,
        }
    ],




    # method 8
    # 2013-01-03
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 99Dh
            b"\x74\xAB"                  # 5  jz short loc_81224B
            b"\x83\xE8\xAB"              # 7  sub eax, 3
            b"\x74\xAB"                  # 10 jz short loc_81223D
            b"\xE8"                      # 12 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "jz1": (6, 1, False),
            "sub2": (9, 1),
            "jz2": (11, 1, False),
        },
        {
            "method": 8,
            "fixedOffset": 0,
            "ignore": False,
            "next": 17,
        }
    ],
    # 2010-08-03
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 2CAh
            b"\x74\xAB"                  # 5  jz short loc_60DA85
            b"\x83\xE8\xAB"              # 7  sub eax, 21h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 10 jz loc_60DBC5
            b"\xE8"                      # 16 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "jz1": (6, 1, False),
            "jz2": (12, False),
        },
        {
            "method": 8,
            "fixedOffset": 0,
            "ignore": False,
            "next": 16,
        }
    ],




    # method 9
    # 2013-02-20
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 99Dh
            b"\x74\xAB"                  # 5  jz short loc_7FD721
            b"\x83\xE8\xAB"              # 7  sub eax, 3
            b"\x74\xAB"                  # 10 jz short loc_7FD713
            b"\x83\xE8\xAB"              # 12 sub eax, 5
            b"\x74\xAB"                  # 15 jz short loc_7FD705
            b"\xE8"                      # 17 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": (14, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jz3": (16, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 17,
        }
    ],
    # 2010-06-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 448h
            b"\x74\xAB"                  # 5  jz short loc_60AC3A
            b"\x2D\xAB\xAB\xAB\xAB"      # 7  sub eax, 3B7h
            b"\x74\xAB"                  # 12 jz short loc_60AC2A
            b"\x83\xE8\xAB"              # 14 sub eax, 3Fh
            b"\x74\xAB"                  # 17 jz short loc_60AC0C
            b"\xE8"                      # 19 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": 8,
            "sub3": (16, 1),
            "jz1": (6, 1, False),
            "jz2": (13, 1, False),
            "jz3": (18, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 19,
        }
    ],
    # 2009-01-14
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 2CAh
            b"\x74\xAB"                  # 5  jz short loc_5DB154
            b"\x83\xE8\xAB"              # 7  sub eax, 21h
            b"\x74\xAB"                  # 10 jz short loc_5DB199
            b"\x2D\xAB\xAB\xAB\xAB"      # 12 sub eax, 0FCh
            b"\x74\xAB"                  # 17 jz short loc_5DB184
            b"\xE8"                      # 19 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": 13,
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jz3": (18, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 19,
        }
    ],
    # 2009-02-25
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 2CAh
            b"\x74\xAB"                  # 5  jz short loc_5DA5BB
            b"\x83\xE8\xAB"              # 7  sub eax, 21h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 10 jz loc_5DA600
            b"\x2D\xAB\xAB\xAB\xAB"      # 16 sub eax, 15Dh
            b"\x74\xAB"                  # 21 jz short loc_5DA55E
            b"\xE8"                      # 23 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": 17,
            "jz1": (6, 1, False),
            "jz2": (12, False),
            "jz3": (22, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 23,
        }
    ],
    # 2009-03-18
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5DE2BC
            b"\x2D\xAB\xAB\xAB\xAB"      # 11 sub eax, 13Eh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 16 jz loc_5DE2A7
            b"\x83\xE8\xAB"              # 22 sub eax, 1Fh
            b"\x74\xAB"                  # 25 jz short loc_5DE22A
            b"\xE8"                      # 27 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": 12,
            "sub3": (24, 1),
            "jz1": (7, False),
            "jz2": (18, False),
            "jz3": (26, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 27,
        }
    ],
    # 2009-03-25
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5DFF8C
            b"\x83\xE8\xAB"              # 11 sub eax, 14h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 14 jz loc_5DFF77
            b"\x2D\xAB\xAB\xAB\xAB"      # 20 sub eax, 149h
            b"\x74\xAB"                  # 25 jz short loc_5DFEFA
            b"\xE8"                      # 27 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (13, 1),
            "sub3": 21,
            "jz1": (7, False),
            "jz2": (16, False),
            "jz3": (26, 1, False),
        },
        {
            "method": 9,
            "fixedOffset": 0,
            "ignore": False,
            "next": 27,
        }
    ],



    # method 10
    # 2010-01-05
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5FDD5C
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 448h
            b"\x74\xAB"                  # 16 jz short loc_5FDCC7
            b"\x3D\xAB\xAB\xAB\xAB"      # 18 cmp eax, 7FFh
            b"\x74\xAB"                  # 23 jz short loc_5FDCB7
            b"\xE8"                      # 25 call CRagConnection_instanceR
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "cmp3": 19,
            "jz1": (7, False),
            "jz2": (17, 1, False),
            "jz3": (24, 1, False),
        },
        {
            "method": 10,
            "fixedOffset": 0,
            "ignore": False,
            "next": 25,
        }
    ],
    # 2009-02-25
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5DD17E
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 374h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 16 jz loc_5DD169
            b"\x3D\xAB\xAB\xAB\xAB"      # 22 cmp eax, 448h
            b"\x74\xAB"                  # 27 jz short loc_5DD0EC
            b"\xE8"                      # 29 call CRagConnection_instanceR
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "cmp3": 23,
            "jz1": (7, False),
            "jz2": (18, False),
            "jz3": (28, 1, False),
        },
        {
            "method": 10,
            "fixedOffset": 0,
            "ignore": False,
            "next": 29,
        }
    ],





    # method 11
    # 2010-01-05
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 7E8h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5FEEEC
            b"\x48"                      # 11 dec eax
            b"\x74\xAB"                  # 12 jz short loc_5FEE8E
            b"\x83\xE8\xAB"              # 14 sub eax, 16h
            b"\x74\xAB"                  # 17 jz short loc_5FEE7B
            b"\xE8"                      # 19 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": (16, 1),
            "jz1": (7, False),
            "jz2": (13, 1, False),
            "jz3": (18, 1, False),
        },
        {
            "method": 11,
            "fixedOffset": 0,
            "ignore": False,
            "next": 19,
        }
    ],


    # method 12
    # 2010-07-20
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 448h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 5  jnz loc_60B468
        ),
        {
            "cmp": 1,
            "jnz": (7, False),
        },
        {
            "method": 12,
            "fixedOffset": 0,
            "ignore": False,
            "next": 11,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 75h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 3  jnz loc_5C9386
        ),
        {
            "cmp": (2, 1),
            "jnz": (5, False),
        },
        {
            "method": 12,
            "fixedOffset": 0,
            "ignore": False,
            "next": 9,
        }
    ],
    # 2010-10-19
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 828h
            b"\x75\xAB"                  # 5  jnz short loc_64244D
        ),
        {
            "cmp": 1,
            "jnz": (6, 1, False),
        },
        {
            "method": 12,
            "fixedOffset": 0,
            "ignore": False,
            "next": 7,
        }
    ],





    # method 13
    # 2010-07-20
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 448h
            b"\x74\xAB"                  # 5  jz short loc_60FD2F
            b"\x3D\xAB\xAB\xAB\xAB"      # 7  cmp eax, 7FFh
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 12 jnz loc_60FDF1
        ),
        {
            "cmp1": 1,
            "cmp2": 8,
            "jz": (6, 1, False),
            "jnz": (14, False),
        },
        {
            "method": 13,
            "fixedOffset": 0,
            "ignore": False,
            "next": 18,
        }
    ],
    # 2019-06-05
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_A63CC7
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 448h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 16 jnz loc_A63CA8
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "jz": (7, False),
            "jnz": (18, False),
        },
        {
            "method": 13,
            "fixedOffset": 0,
            "ignore": False,
            "next": 22,
        }
    ],
    # 2008-11-19
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 0E0AAAA7Ch
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5CB27D
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 0F8AAAA6Fh
            b"\x75\xAB"                  # 16 jnz short loc_5C2524
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "jz": (7, False),
            "jnz": (17, 1, False),
        },
        {
            "method": 13,
            "fixedOffset": 0,
            "ignore": False,
            "next": 18,
        }
    ],




    # method 14
    # 2010-08-03
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 7FFh
            b"\x74\xAB"                  # 5  jz short loc_60DB13
            b"\x83\xE8\xAB"              # 7  sub eax, 27h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 10 jnz loc_60DA57
            b"\x8D\x8D"                  # 16 lea ecx, [ebp+var_5004]
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "jz": (6, 1, False),
            "jnz": (12, False),
        },
        {
            "method": 14,
            "fixedOffset": 0,
            "ignore": False,
            "next": 16,
        }
    ],
    # 2010-08-17
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 7FFh
            b"\x74\xAB"                  # 5  jz short loc_624E20
            b"\x83\xE8\xAB"              # 7  sub eax, 27h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 10 jnz loc_624D6E
            b"\x68"                      # 16 push offset word_85B0B0
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "jz": (6, 1, False),
            "jnz": (12, False),
        },
        {
            "method": 14,
            "fixedOffset": 0,
            "ignore": False,
            "next": 16,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 290h
            b"\x74\xAB"                  # 5  jz short loc_60B370
            b"\x83\xE8\xAB"              # 7  sub eax, 3Ah
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 10 jnz loc_60B468
            b"\x8D\x85"                  # 16 lea eax, [ebp+var_5004]
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "jz": (6, 1, False),
            "jnz": (12, False),
        },
        {
            "method": 14,
            "fixedOffset": 0,
            "ignore": False,
            "next": 16,
        }
    ],




    # method 15
    # 2009-01-07
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 1C7h
            b"\x74\xAB"                  # 5  jz short loc_5DAD6C
            b"\x83\xE8\xAB"              # 7  sub eax, 15h
            b"\x74\xAB"                  # 10 jz short loc_5DAD59
            b"\x83\xE8\xAB"              # 12 sub eax, 15h
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 15 jnz loc_5DAE46
            b"\x8D\x95"                  # 21 lea edx, [ebp+buf]
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": (14, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jnz": (17, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 21,
        }
    ],
    # 2009-03-11
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 1DCh
            b"\x74\xAB"                  # 5  jz short loc_5DDFC0
            b"\x83\xE8\xAB"              # 7  sub eax, 15h
            b"\x74\xAB"                  # 10 jz short loc_5DDFAD
            b"\x83\xE8\xAB"              # 12 sub eax, 1Ch
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 15 jnz loc_5DE066
            b"\xB9"                      # 21 mov ecx, 36h
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": (14, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jnz": (17, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 21,
        }
    ],
    # 2008-01-24
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 1DCh
            b"\x74\xAB"                  # 5  jz short loc_5BD13A
            b"\x83\xE8\xAB"              # 7  sub eax, 15h
            b"\x74\xAB"                  # 10 jz short loc_5BD127
            b"\x83\xE8\xAB"              # 12 sub eax, 1Ch
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 15 jnz loc_5BD1D8
            b"\x8B\x95"                  # 21 mov edx, [ebp+var_80C+2]
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": (14, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jnz": (17, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 21,
        }
    ],
    # 2008-04-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 0C4h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5C4134
            b"\x2D\xAB\xAB\xAB\xAB"      # 11 sub eax, 0C3h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 16 jz loc_5C4112
            b"\x83\xE8\xAB"              # 22 sub eax, 2Eh
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 25 jnz loc_5C4106
            b"\x8D\x85"                  # 31 lea eax, [ebp+var_80C]
        ),
        {
            "sub1": 1,
            "sub2": 12,
            "sub3": (24, 1),
            "jz1": (7, False),
            "jz2": (18, False),
            "jnz": (27, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 31,
        }
    ],
    # 2008-07-15
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 283h
            b"\x74\xAB"                  # 5  jz short loc_5D45F2
            b"\x83\xE8\xAB"              # 7  sub eax, 8
            b"\x74\xAB"                  # 10 jz short loc_5D45A3
            b"\x83\xE8\xAB"              # 12 sub eax, 3
            b"\x75\xAB"                  # 15 jnz short loc_5D45D6
            b"\x8D\x95"                  # 17 lea edx, [ebp+var_80C]
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "sub3": (14, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jnz": (16, 1, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 17,
        }
    ],
    # 2009-02-25
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 283h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5DA5C9
            b"\x83\xE8\xAB"              # 11 sub eax, 8
            b"\x74\xAB"                  # 14 jz short loc_5DA513
            b"\x83\xE8\xAB"              # 16 sub eax, 3
            b"\x75\xAB"                  # 19 jnz short loc_5DA550
            b"\x8D\x95"                  # 21 lea edx, [ebp+var_138C]
        ),
        {
            "sub1": 1,
            "sub2": (13, 1),
            "sub3": (18, 1),
            "jz1": (7, False),
            "jz2": (15, 1, False),
            "jnz": (20, 1, False),
        },
        {
            "method": 15,
            "fixedOffset": 0,
            "ignore": False,
            "next": 21,
        }
    ],




    # method 16
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x74\xAB"                  # 5  jz short loc_5F0B5F
            b"\xE8"                      # 7  call CRagConnection_instanceR
        ),
        {
            "cmp": 1,
            "jz": (6, 1, False),
        },
        {
            "method": 16,
            "fixedOffset": 0,
            "ignore": False,
            "next": 7
        }
    ],




    # method 17
    # 2009-03-12
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x74\xAB"                  # 5  jz short loc_609BFA
            b"\x3D\xAB\xAB\xAB\xAB"      # 7  cmp eax, 448h
            b"\x74\xAB"                  # 12 jz short loc_609B9D
            b"\xE8"                      # 14 call CRagConnection_instanceR
        ),
        {
            "cmp1": 1,
            "cmp2": 8,
            "jz1": (6, 1, False),
            "jz2": (13, 1, False),
        },
        {
            "method": 17,
            "fixedOffset": 0,
            "ignore": False,
            "next": 14
        }
    ],



    # method 18
    # 2009-09-22
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"      # 0  sub eax, 448h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_5F12EB
            b"\x2D\xAB\xAB\xAB\xAB"      # 11 sub eax, 3A0h
            b"\x74\xAB"                  # 16 jz short loc_5F129B
            b"\x48"                      # 18 dec eax
            b"\x74\xAB"                  # 19 jz short loc_5F1249
            b"\xE8"                      # 21 call CRagConnection_instanceR
        ),
        {
            "sub1": 1,
            "sub2": 12,
            "jz1": (7, False),
            "jz2": (17, 1, False),
            "jz3": (20, 1, False),
        },
        {
            "method": 18,
            "fixedOffset": 0,
            "ignore": False,
            "next": 21
        }
    ],



    # method 19
    # 2008-06-17
    [
        (
            b"\x83\xE8\xAB"              # 0  sub eax, 77h
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 3  jz loc_5C9392
            b"\x48"                      # 9  dec eax
            b"\x74\xAB"                  # 10 jz short loc_5BDA03
            b"\x48"                      # 12 dec eax
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 13 jnz loc_5C9386
            b"\x8D\x95"                  # 19 lea edx, [ebp+var_6024]
        ),
        {
            "sub1": (2, 1),
            "jz1": (5, False),
            "jz2": (11, 1, False),
            "jnz": (15, False),
        },
        {
            "method": 19,
            "fixedOffset": 0,
            "ignore": False,
            "next": 19
        }
    ],



    # method 20
    # 2008-11-19
    [
        (
            b"\x83\xE8\xAB"              # 0  sub eax, 75h
            b"\x74\xAB"                  # 3  jz short loc_5C2530
            b"\x48"                      # 5  dec eax
            b"\x74\xAB"                  # 6  jz short loc_5C2530
        ),
        {
            "sub1": (2, 1),
            "jz1": (4, 1, False),
            "jz2": (7, 1, False),
        },
        {
            "method": 20,
            "fixedOffset": 0,
            "ignore": False,
            "next": 8,
        }
    ],




    # method 21
    # 2010-08-17
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_643AE0
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 448h
            b"\x74\xAB"                  # 16 jz short loc_643A1D
            b"\x3D\xAB\xAB\xAB\xAB"      # 18 cmp eax, 7FFh
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 23 jnz loc_643AA7
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "cmp3": 19,
            "jz1": (7, False),
            "jz2": (17, 1, False),
            "jnz": (25, False),
        },
        {
            "method": 21,
            "fixedOffset": 0,
            "ignore": False,
            "next": 29,
        }
    ],



    # method 22
    # aro
    [
        (
            b"\x3D\xAB\xAB\xAB\xAB"      # 0  cmp eax, 2EBh
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_710192
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 448h
            b"\x74\xAB"                  # 16 jz short loc_7100EC
            b"\x3D\xAB\xAB\xAB\xAB"      # 18 cmp eax, 7FFh
            b"\x75\xAB"                  # 23 jnz short loc_71015B
        ),
        {
            "cmp1": 1,
            "cmp2": 12,
            "cmp3": 19,
            "jz1": (7, False),
            "jz2": (17, 1, False),
            "jnz": (24, 1, False),
        },
        {
            "method": 22,
            "fixedOffset": 0,
            "ignore": False,
            "next": 25,
        }
    ],




    # method 23
    # euro 2018-05-08
    [
        (
            b"\x2D\xAB\xAB\xAB\xAB"     # 0  sub eax, 82Ah
            b"\x74\xAB"                 # 5  jz short loc_852FD0
            b"\x83\xE8\xAB"             # 7  sub eax, 2
            b"\x74\xAB"                 # 10 jz short loc_852FC2
            b"\x48"                     # 12 dec eax
            b"\x75\xAB"                 # 13 jnz short loc_853000
            b"\x68\xAB\xAB\xAB\xAB"     # 15 push offset unk_D83D90
        ),
        {
            "sub1": 1,
            "sub2": (9, 1),
            "jz1": (6, 1, False),
            "jz2": (11, 1, False),
            "jnz": (14, 1, False),
        },
        {
            "method": 23,
            "fixedOffset": 0,
            "ignore": False,
            "next": 15,
        }
    ],






    # ignore
    # 2019-06-05
    [
        (
            b"\x6A\xAB"                  # 0  push 0
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2019-06-05
    [
        (
            b"\x0F\xB6\x05"  # 0  movzx eax, packet_buf+2
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2019-06-05
    [
        (
            b"\x68"      # 0  push offset packet_buf
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2019-06-05
    [
        (
            b"\xA1"      # 0  mov eax, dword ptr packet_buf+2
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2019-06-05
    [
        (
            b"\xE8"      # 0  call CRagConnection_instanceR
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2015-01-07
    [
        (
            b"\x53"                      # 0  push ebx
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-01-08
    [
        (
            b"\xE9"      # 0  jmp loc_D2568D
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-01-15
    [
        (
            b"\x83\x3D"  # 0  cmp dword_C82AF4, 0
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-01-22
    [
        (
            b"\xBA"      # 0  mov edx, 85Dh
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-03-05
    [
        (
            b"\xEB"                  # 0  jmp short loc_83F88D
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-03-19
    [
        (
            b"\x8D\x45"              # 0  lea eax, [ebp+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-03-19
    [
        (
            b"\x8D\x4D"              # 0  lea ecx, [ebp+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-04-23
    [
        (
            b"\x8D\x55"              # 0  lea edx, [ebp+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-05-28
    [
        (
            b"\xB8"      # 0  mov eax, 369h
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2014-08-13
    [
        (
            b"\xB9"      # 0  mov ecx, 8A2h
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2013-01-15
    [
        (
            b"\x8D\x54"          # 0  lea edx, [esp+94h+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2013-01-30
    [
        (
            b"\x8D\x4C"          # 0  lea ecx, [esp+94h+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2013-03-06
    [
        (
            b"\x39\x2D"  # 0  cmp dword_A4F5B4, ebp
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2013-03-27
    [
        (
            b"\x8D\x44"          # 0  lea eax, [esp+94h+var_80]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-01-05
    [
        (
            b"\x8D\x85"  # 0  lea eax, [ebp+var_5004]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-01-05
    [
        (
            b"\x8D\x95"  # 0  lea edx, [ebp+var_5004]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-01-05
    [
        (
            b"\x8B\x8D"  # 0  mov ecx, dword ptr [ebp+var_5004+2
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-01-05
    [
        (
            b"\x0F\xBF"  # 0  movsx esi, word ptr [ebp+var_5
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-01-05
    [
        (
            b"\x8A\x85"  # 0  mov al, [ebp-5004h]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-06-15
    [
        (
            b"\x8D\x8D"  # 0  lea ecx, [ebp+var_5004]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-07-20
    [
        (
            b"\x8B\x85"  # 0  mov eax, [ebp+var_5004+2]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-08-03
    [
        (
            b"\x8B\x95"  # 0  mov edx, [ebp+var_5004+2]
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-08-17
    [
        (
            b"\x33\xC9"                  # 0  xor ecx, ecx
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2010-10-19
    [
        (
            b"\x8B\x0D"  # 0  mov ecx, dword_89D2C2
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2009-02-11
    [
        (
            b"\x56"                      # 0  push esi
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
    # 2008-06-17
    [
        (
            b"\x33\xD2"                  # 0  xor edx, edx
        ),
        {
        },
        {
            "method": "ignore",
            "fixedOffset": 0,
            "ignore": True,
        }
    ],
]
